package com.systema.pixona.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class RestartServiceReceiver extends BroadcastReceiver
{

    private static final String TAG = RestartServiceReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive Of Broadcast");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent1 = new Intent(context, UploadService.class);
            intent.putExtra("file_path", intent.getStringExtra("file_path"));
            context.startForegroundService(intent1);
        } else {
            Intent intent1 = new Intent(context, UploadService.class);
            intent.putExtra("file_path", intent.getStringExtra("file_path"));
            context.startService(intent1);
        }


    }

}