package com.systema.pixona.Services;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.SharedPreference;
import com.systema.pixona.Utils.Util;

import java.io.File;
import java.util.List;

public class UploadService extends Service  {

    private String TAG = "UPLOAD_SERVICE";
    private TransferUtility transferUtility;
    private List<TransferObserver> observers;

    private LocalBroadcastManager localBroadcastManager;
    public static final String SERVICE_RESULT = "com.service.result";
    public static final String SERVICE_MESSAGE = "com.service.message";
    private String filePath;
    private Context context;

    public UploadService(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startMyOwnForeground();
        else
            startForeground(1, new Notification());

        Util util = new Util();
        transferUtility = util.getTransferUtility(this);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    private void sendResult(String error) {
        Intent intent = new Intent(SERVICE_RESULT);
        intent.putExtra(SERVICE_MESSAGE, "onError");
        intent.putExtra("msg", error);
        localBroadcastManager.sendBroadcast(intent);
    }


    private void sendResult(long current, long total) {
        Intent intent = new Intent(SERVICE_RESULT);
        intent.putExtra(SERVICE_MESSAGE, "onProgressChanged");
        intent.putExtra("current", current);
        intent.putExtra("total", total);
        localBroadcastManager.sendBroadcast(intent);
    }


    private void sendResult(TransferState state) {
        Intent intent = new Intent(SERVICE_RESULT);
        intent.putExtra(SERVICE_MESSAGE, "onStateChanged");
        intent.putExtra("state", state);
        localBroadcastManager.sendBroadcast(intent);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void startMyOwnForeground() {
        String NOTIFICATION_CHANNEL_ID = "com.systema";
        String channelName = "Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle("App is running in background")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            filePath = intent.getStringExtra("file_path");

            File file = new File(filePath);
            TransferObserver observer = transferUtility.upload(
                    Constants.BUCKET_NAME + SharedPreference.getLoginUserEmail(context), //Bucket Name
                    String.valueOf(System.currentTimeMillis())+".mp4", //File Name in bucket ~ //file.getName() or //System.currentTimeMillis()
                    file //File you want to upload to bucket
            );

            //             * Note that usually we set the transfer listener after initializing the
//             * transfer. However it isn't required in this sample app. The flow is
//             * click upload button -> start an activity for image selection
//             * startActivityForResult -> onActivityResult -> beginUpload -> onResume
//             * -> set listeners to in progress transfers.

            observer.setTransferListener(new UploadListener());

        }

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    public class UploadListener implements TransferListener {

        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "Error during upload: " + id, e);

            sendResult(e.getMessage());

        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d", id, bytesTotal, bytesCurrent));

            sendResult(bytesCurrent, bytesTotal);

            Log.e("AAAAAAAAA", String .format("Uploaded %d of %d", bytesCurrent, bytesTotal ));

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.d(TAG, "onStateChanged: " + id + ", " + newState);

            if (newState.name().equalsIgnoreCase("completed")) {
                sendResult(newState);
                Toast.makeText(UploadService.this, "Uploaded...", Toast.LENGTH_SHORT).show();
                Log.e("AAAAAAAAA", "Uploaded...");
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("AAAAAAAAA", "Restarted...");

        Intent intent1 = new Intent("YouWillNeverKillMe");
        intent1.putExtra("file_path", filePath);
        intent1.setClass(this, RestartServiceReceiver.class);
        sendBroadcast(intent1);

    }
}
