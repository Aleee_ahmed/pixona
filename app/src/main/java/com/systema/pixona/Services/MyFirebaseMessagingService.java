package com.systema.pixona.Services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Util;
import com.systema.pixona.Views.activities.IdeasMenu;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FirebaseMesagingService";
    private int i;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());

        }
    }

    private void handleNotification(String message) {
        if (!Util.isAppIsInBackground(getApplicationContext())) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationForOreo("Xunami", message, i, R.drawable.logo_50);
            } else {
                NotificationBelowOreo("Xunami", message, i, R.drawable.logo_50);
            }


        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void  NotificationForOreo(String Title, String msg, int id, int Icon) {

        String cid = "my_channel_01";

        CharSequence name ="Xunami Notification Channel";


        String description = "Xunami video submission app";

        int importance = NotificationManager.IMPORTANCE_HIGH;

        NotificationChannel mChannel = new NotificationChannel(cid, name, importance);

        mChannel.setDescription(description);

        mChannel.enableLights(true);

        mChannel.setLightColor(Color.RED);

        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

        Intent intent = new Intent(getApplicationContext(), IdeasMenu.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, i, intent, 0);


        Notification.Builder builder = new Notification.Builder(getApplicationContext())
                .setContentTitle(Title)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(Icon)
                .setNumber(i)
                .setDefaults(Notification.DEFAULT_ALL)
                .setChannelId(cid);


        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(mChannel);
        notificationManager.notify(id, notification);

        i = i + 1;

    }

    public void NotificationBelowOreo(String Title, String msg, int id, int Icon) {

        Intent intent = new Intent(getApplicationContext(), IdeasMenu.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, i, intent, 0);

        Notification.Builder builder = new Notification.Builder(getApplicationContext())
                .setContentTitle(Title)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(Icon)
                .setNumber(i)
                .setDefaults(Notification.DEFAULT_ALL);


        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify(id, notification);

        i = i + 1;

    }

}
