package com.systema.pixona.Presenter;

import com.systema.pixona.Interfaces.IVideoDetails;
import com.systema.pixona.Models.VideoDetailsModel;
import com.systema.pixona.Network.RetrofitClient;
import com.systema.pixona.Network.RetrofitInterface;
import com.systema.pixona.Utils.Constants;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;

public class VideoDetailsPresenter implements IVideoDetails.iVideoDetailsPresenter {

    private IVideoDetails.iVideoDetailsMain iVideoDetailsMain;

    public VideoDetailsPresenter(IVideoDetails.iVideoDetailsMain iVideoDetailsMain) {
        this.iVideoDetailsMain = iVideoDetailsMain;
    }

    @Override
    public void fetchVideoDetails(String videoId) {

        iVideoDetailsMain.showProgress();

        RetrofitInterface retrofitInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        Observable<VideoDetailsModel> observable =
                retrofitInterface.getVideoDetails(Constants.decode(videoId), Constants.VALUES.PART, Constants.Youtube_API_KEY);

        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DefaultObserver<VideoDetailsModel>() {
                    @Override
                    public void onNext(VideoDetailsModel videoDetailsModel) {
                        iVideoDetailsMain.displayAddTravelPlan(videoDetailsModel);
                    }

                    @Override
                    public void onError(Throwable e) {
                        iVideoDetailsMain.hideProgress();
                        iVideoDetailsMain.displayError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
}
