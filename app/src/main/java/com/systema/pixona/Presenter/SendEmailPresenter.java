package com.systema.pixona.Presenter;

import com.systema.pixona.Interfaces.ISendEmail;
import com.systema.pixona.Network.RetrofitClient;
import com.systema.pixona.Network.RetrofitInterface;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;

public class SendEmailPresenter implements ISendEmail.iSendEmailPresenter {

    private ISendEmail.iSendEmailMain main;

    public SendEmailPresenter(ISendEmail.iSendEmailMain main) {
        this.main = main;
    }

    @Override
    public void sendEmail(String email, String subject, String message) {

        main.showProgress();


        RetrofitInterface retrofitInterface = RetrofitClient.getEmailClient().create(RetrofitInterface.class);

        Observable<String> observable = retrofitInterface
                .sendEmail(
                        email,
                        subject,
                        message
                );

        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DefaultObserver<String>() {
                    @Override
                    public void onNext(String model) {
                        main.displayResult(model);
                    }

                    @Override
                    public void onError(Throwable e) {
                        main.hideProgress();
                        main.displayError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        main.hideProgress();
                    }
                });

    }
}
