package com.systema.pixona.Views.activities;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.systema.pixona.R;
import com.systema.pixona.Utils.MirroredTextView;

import top.defaults.camera.CameraView;
import top.defaults.camera.CanvasDrawer;
import top.defaults.camera.Error;
import top.defaults.camera.Photographer;
import top.defaults.camera.PhotographerFactory;
import top.defaults.camera.PhotographerHelper;
import top.defaults.camera.SimpleOnEventListener;
import top.defaults.camera.Utils;

public class TestCamera extends AppCompatActivity {

    private Photographer photographer;
    private boolean r = false;
    public static final String MEDIA_DIR = Environment.getExternalStorageDirectory().getPath() + "/0/dev/Pixona";

    private float size = 24f;

    private View view;
    private Handler customHandler;
    private boolean delayDone;
    private Runnable scroll = new scrollRunnable();
    private int scrollSpeed;
    private boolean scrollText;
    private int time;
    private int timer;
    private SeekBar seekBar;
    private TextView textViewToChange;
    private ScrollView scrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_camera);


        String description = getResources().getString(R.string.script);

        CameraView preview = findViewById(R.id.preview);

        preview.setFocusIndicatorDrawer(new CanvasDrawer() {
            private static final int SIZE = 300;
            private static final int LINE_LENGTH = 50;

            @Override
            public Paint[] initPaints() {
                Paint focusPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
                focusPaint.setStyle(Paint.Style.STROKE);
                focusPaint.setStrokeWidth(2);
                focusPaint.setColor(Color.WHITE);
                return new Paint[]{ focusPaint };
            }

            @Override
            public void draw(Canvas canvas, Point point, Paint[] paints) {
                if (paints == null || paints.length == 0) return;

                int left = point.x - (SIZE / 2);
                int top = point.y - (SIZE / 2);
                int right = point.x + (SIZE / 2);
                int bottom = point.y + (SIZE / 2);

                Paint paint = paints[0];

                canvas.drawLine(left, top + LINE_LENGTH, left, top, paint);
                canvas.drawLine(left, top, left + LINE_LENGTH, top, paint);

                canvas.drawLine(right - LINE_LENGTH, top, right, top, paint);
                canvas.drawLine(right, top, right, top + LINE_LENGTH, paint);

                canvas.drawLine(right, bottom - LINE_LENGTH, right, bottom, paint);
                canvas.drawLine(right, bottom, right - LINE_LENGTH, bottom, paint);

                canvas.drawLine(left + LINE_LENGTH, bottom, left, bottom, paint);
                canvas.drawLine(left, bottom, left, bottom - LINE_LENGTH, paint);
            }
        });

        photographer = PhotographerFactory.createPhotographerWithCamera2(this, preview);


        PhotographerHelper photographerHelper = new PhotographerHelper(photographer); // init with photographer
        photographerHelper.setFileDir(MEDIA_DIR); // set directory for image/video saving

//        photographerHelper.flip(); // flip back/front camera
//        photographerHelper.switchMode();
//

        photographer.setOnEventListener(new SimpleOnEventListener() {
            @Override
            public void onDeviceConfigured() {}

            @Override
            public void onPreviewStarted() {}

            @Override
            public void onZoomChanged(float zoom) {}

            @Override
            public void onPreviewStopped() {}

            @Override
            public void onStartRecording() {
                Toast.makeText(TestCamera.this, "Recording started..", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinishRecording(String filePath) {
                announcingNewFile(filePath);
            }

            @Override
            public void onShotFinished(String filePath) {}

            @Override
            public void onError(Error error) {
                super.onError(error);
            }
        });

        findViewById(R.id.filp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photographerHelper.flip();
            }
        });

        findViewById(R.id.mode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!r) {
                    photographer.startRecording(null);
                    r = true;
                } else {
                    finishRecordingIfNeeded();
                    r = false;
                }
            }
        });


        seekBar = findViewById(R.id.camera_SeekBar);


        textViewToChange = findViewById(R.id.TextTeleprompter);

        textViewToChange.setLineSpacing((float) (11), 1.0f);
        scrollSpeed = 21;
        MirroredTextView.mirror = false;
        textViewToChange.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto.ttf"), Typeface.NORMAL);
        textViewToChange.setTextSize(size);
        textViewToChange.setText(description);
        textViewToChange.setTextColor(Color.parseColor("#FFFFFF"));
        textViewToChange.setBackgroundColor(Color.parseColor("#700A0A0A"));

        time = scrollSpeed * 100;
        scrollText = false;
        delayDone = true;

        scrollView = findViewById(R.id.ScrollerTeleprompter);
        scrollView.setBackgroundColor(Color.parseColor("#700A0A0A"));
        scrollView.getChildAt(0).setOnClickListener(new clicked());

        customHandler = new Handler(Looper.getMainLooper());

        int max = 49;
        int min = 3;
        seekBar.setMax(max - min);
        seekBar.setProgress(scrollSpeed);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)  {
                scrollSpeed = max - seekBar.getProgress();
                time = scrollSpeed * 100;
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        enterFullscreen();
        photographer.startPreview();
    }

    @Override
    protected void onPause() {
        photographer.stopPreview();
        super.onPause();
    }

    private void enterFullscreen() {
        View decorView = getWindow().getDecorView();
        decorView.setBackgroundColor(Color.BLACK);
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }

    private void finishRecordingIfNeeded() {
        if (r) {
            photographer.finishRecording();
        }
    }

    private void announcingNewFile(String filePath) {
        Toast.makeText(TestCamera.this, "File: " + filePath, Toast.LENGTH_SHORT).show();
        Utils.addMediaToGallery(TestCamera.this, filePath);
    }



    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 1) {
            tap();
        }
        return true;
    }

    private void tap() {
        if (delayDone) {
            scrollText = !scrollText;
            customHandler.post(scroll);
        }
    }


    class scrollRunnable implements Runnable {
        scrollRunnable() {}

        public void run() {
//            Log.d(TAG, "run() called");
            if (scrollText) {
                if (timer > 0) {
                    timer = timer - 1;
                } else {
                    scrollView.scrollTo(0, scrollView.getScrollY() + 1);
                    timer = time;
                }
                customHandler.post(scroll);
            }
        }
    }

    class clicked implements View.OnClickListener {
        clicked() {
        }
        public void onClick(View view) {
            tap();
        }
    }




}
