package com.systema.pixona.Views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.systema.pixona.Models.UsersModel;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Views.adapter.UsersAdapter;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class SearchUsers extends AppCompatActivity {

    private EditText etSearch;
    private List<UsersModel> arrayList;
    private TextView tvActiveUsers, tvPendingUsers, tvDeactiveUsers;
    private RecyclerView recyclerView;
    private UsersAdapter adapter;
    private LinearLayout linearLayout;
    private MKLoader loader;
    private int active = 0, pending = 0, deactive = 0;

    private void init() {
        etSearch = findViewById(R.id.searchUsers_etSearch);
        tvActiveUsers = findViewById(R.id.searchUsers_tvActive);
        tvPendingUsers = findViewById(R.id.searchUsers_tvPending);
        tvDeactiveUsers = findViewById(R.id.searchUsers_tvDeactive);
        linearLayout = findViewById(R.id.searchUsers_llSearchUsers);
        loader = findViewById(R.id.searchUsers_loader);

        arrayList = new ArrayList<>();
        recyclerView = findViewById(R.id.searchUsers_rvUsers);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new UsersAdapter(arrayList);
        recyclerView.setAdapter(adapter);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_users);

        init();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        arrayList.clear();
        active = pending = deactive = 0;
        getUsers();
    }

    private void getUsers() {
        arrayList.clear();
        
        FirebaseDatabase.getInstance().getReference("users")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String status;
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            status = snapshot.child("status").getValue(String.class);
                            arrayList.add(new UsersModel(
                               snapshot.child("name").getValue(String.class),
                               Constants.decode(snapshot.getKey()),
                               snapshot.child("joining_date").getValue(String.class),
                               snapshot.child("expiration_date").getValue(String.class),
                               snapshot.child("credit_card_number").getValue(String.class),
                               snapshot.child("ccv").getValue(String.class),
                               status,
                               snapshot.child("zip_code").getValue(String.class)
                             ));
                            if (Objects.requireNonNull(status).equalsIgnoreCase(Constants.ACTIVE)) {
                                ++active;
                            } else if (status.equalsIgnoreCase(Constants.PENDING)) {
                                ++pending;
                            } else {
                                ++deactive;
                            }
                        }
                        adapter = new UsersAdapter(arrayList);
                        recyclerView.setAdapter(adapter);
                        tvActiveUsers.setText(String.format(Locale.ENGLISH, "%d", active));
                        tvPendingUsers.setText(String.format(Locale.ENGLISH, "%d", pending));
                        tvDeactiveUsers.setText(String.format(Locale.ENGLISH, "%d", deactive));
                        loader.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        loader.setVisibility(View.GONE);
                        Toast.makeText(SearchUsers.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }
}
