package com.systema.pixona.Views.adapter;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.systema.pixona.Models.UsersModel;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Views.activities.UserDetails;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> implements Filterable {

    private List<UsersModel> arrayList;
    private List<UsersModel> arrayListFiltered;

    public UsersAdapter(List<UsersModel> arrayList) {
        this.arrayList = arrayList;
        this.arrayListFiltered = new ArrayList<>(arrayList);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.si_users, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvName.setText(arrayListFiltered.get(holder.getAdapterPosition()).getName());
        holder.tvDate.setText(arrayListFiltered.get(holder.getAdapterPosition()).getJoiningDate());

        int drawable;
        if (arrayListFiltered.get(holder.getAdapterPosition()).getStatus().equalsIgnoreCase(Constants.ACTIVE)) {
            drawable = R.drawable.dot_green;
        } else if (arrayListFiltered.get(holder.getAdapterPosition()).getStatus().equalsIgnoreCase(Constants.PENDING)) {
            drawable = R.drawable.dot_yellow;
        } else {
            drawable = R.drawable.dot_red;
        }

        holder.ivIndicator.setImageResource(drawable);


        holder.view.setOnClickListener(v -> {
            v.getContext().startActivity(
                    new Intent(v.getContext(), UserDetails.class)
                            .putExtra("name", arrayList.get(holder.getAdapterPosition()).getName())
                            .putExtra("date", arrayList.get(holder.getAdapterPosition()).getJoiningDate())
                            .putExtra("address", arrayList.get(holder.getAdapterPosition()).getZipCode())
                            .putExtra("email", arrayList.get(holder.getAdapterPosition()).getEmail())
                            .putExtra("phone", arrayList.get(holder.getAdapterPosition()).getCreditCardNumber())
                            .putExtra("status", arrayList.get(holder.getAdapterPosition()).getStatus())
                            .putExtra("indicator", drawable)
                    );
        });

    }

    @Override
    public int getItemCount() {
        return arrayListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivIndicator;
        private TextView tvName, tvDate;
        private View view;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            ivIndicator = view.findViewById(R.id.si_users_ivIndicator);
            tvDate = view.findViewById(R.id.si_users_tvDate);
            tvName = view.findViewById(R.id.si_users_tvName);

        }
    }


    private Filter filter = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            if (constraint != null) {
                arrayListFiltered.clear();

                for (UsersModel item : arrayList) {
                    if (item.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        arrayListFiltered.add(item);
                    }
                }

                //Collections.sort(arrayListFiltered, (o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));

                FilterResults filterResults = new FilterResults();
                filterResults.values = arrayListFiltered;
                filterResults.count = arrayListFiltered.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            try {
                if (results != null && results.count > 0) {
                    arrayListFiltered = (ArrayList<UsersModel>) results.values;
                    notifyDataSetChanged();
                }
            } catch (ConcurrentModificationException e) {
                Log.e("COUNTRIES_ADAPTER", "CRASHED: " + e.getLocalizedMessage());
            }
        }

    };

}

