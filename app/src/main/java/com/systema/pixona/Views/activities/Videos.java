package com.systema.pixona.Views.activities;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.systema.pixona.Models.VideosModel;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Internet;
import com.systema.pixona.Views.adapter.VideosAdapter;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class Videos extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MKLoader loader;
    private TextView tvType;
    private VideosAdapter videosAdapter;
    private ArrayList<VideosModel> arrayList;
    private String category;

//    final LinearLayoutManager layoutManager =
//            new LinearLayoutManager(this,
//                    LinearLayoutManager.VERTICAL, false) {
//        @Override
//        public void onLayoutChildren(final RecyclerView.Recycler recycler, final RecyclerView.State state) {
//            super.onLayoutChildren(recycler, state);
//            final int firstVisibleItemPosition = findFirstVisibleItemPosition();
//            if (firstVisibleItemPosition != 0) {
//                if (firstVisibleItemPosition == -1)
//                    //recyclerView.setVisibility(View.GONE);
//                return;
//            }
//            final int lastVisibleItemPosition = findLastVisibleItemPosition();
//            int itemsShown = lastVisibleItemPosition - firstVisibleItemPosition + 1;
//            recyclerView.setVisibility(videosAdapter.getItemCount() > itemsShown ? View.VISIBLE : View.GONE);
//        }
//    };

    private void init() {
        arrayList = new ArrayList<>();
        videosAdapter = new VideosAdapter(arrayList);

        loader = findViewById(R.id.video_loader);
        tvType = findViewById(R.id.video_tvType);

        recyclerView = findViewById(R.id.video_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(videosAdapter);

        category = getIntent().getStringExtra("category");

    }

    private void showProgress() {
        recyclerView.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        recyclerView.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);

        init();

        tvType.setText(String.format(Locale.ENGLISH, "%s Video Ideas", category));

        loadData();
    }

    private void loadData() {

        arrayList.clear();
        videosAdapter.clear();

        if (!Internet.isConnected(this)) {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
            return;
        }

        showProgress();

        FirebaseDatabase.getInstance()
                .getReference("videos")
                .child(category.toLowerCase().replace(" ", "_"))
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    arrayList.add(
                            new VideosModel(
                                    snapshot.getKey(),
                                    snapshot.child("title").getValue(String.class),
                                    snapshot.child("details").getValue(String.class),
                                    snapshot.child("script_url").getValue(String.class),
                                    snapshot.child("shots").getValue(String.class),
                                    snapshot.child("duration").getValue(String.class),
                                    tvType.getText().toString().trim()
                            )
                    );
                }

                hideProgress();
                Collections.shuffle(arrayList);
                videosAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                hideProgress();
                Toast.makeText(Videos.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
