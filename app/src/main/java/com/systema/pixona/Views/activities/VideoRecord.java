package com.systema.pixona.Views.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.systema.pixona.R;
import com.systema.pixona.Views.Fragments.CameraFragment;

public class VideoRecord extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_record);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Bundle bundle = new Bundle();
        bundle.putString("description", getIntent().getStringExtra("description"));

        CameraFragment cameraFragment = new CameraFragment();
        cameraFragment.setArguments(bundle);

        if (null == savedInstanceState) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, cameraFragment)
                    .commit();
        }




    }
}
