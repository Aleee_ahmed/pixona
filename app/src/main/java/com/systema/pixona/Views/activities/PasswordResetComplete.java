package com.systema.pixona.Views.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.systema.pixona.R;

public class PasswordResetComplete extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset_complete);
    }
}
