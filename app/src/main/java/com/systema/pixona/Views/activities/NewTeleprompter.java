package com.systema.pixona.Views.activities;

import android.Manifest;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.systema.pixona.R;
import com.systema.pixona.Utils.DownloadAsyncTask;
import com.systema.pixona.Utils.QueryDeleteHandler;
import com.systema.pixona.Utils.QueryHandler;
import com.systema.pixona.Utils.TeleprompterFile;
import com.systema.pixona.Data.TeleprompterFileContract;

import java.util.ArrayList;
import java.util.List;

import static com.systema.pixona.Utils.Constants.EXTRA_FILE_DATA;

public class NewTeleprompter extends AppCompatActivity implements
        View.OnClickListener,
        TeleprompterFilesAdapter.OnFileClickListener,
        QueryDeleteHandler.onQueryHandlerDeleteComplete,
        LoaderManager.LoaderCallbacks<Cursor>,
        QueryHandler.onQueryHandlerInsertComplete{


    //Top Layout
    LinearLayout llmyScripts,llRecord,llSave;
    ImageView ivMyScripts,ivRecord,ivSave;
    TextView tvMyScripts,tvRecord,tvSave;

    //RecyclerView for my scripts
    RecyclerView rvMyScripts,rvRecordScripts;
    List<TeleprompterFile> teleprompterFileList;


    //Edit Scripts
    View viewEditScripts;
    EditText etTitle,etDetail;
    Button btnSaveScript;


    Boolean isRecordClick=false,isMyScriptFileClick=false, isFileReadyToDownload = false;
    private boolean isFromRecordButton = false;
    LinearLayout llWriteNewScript;
    private Uri mCurrentUri;
    private String TAG = "NewTeleprompter";
    private TeleprompterFilesAdapter mAdapter;
    private TeleprompterFile mDownloadedFile;
    private static final int REQUEST_STORAGE_PERMISSION = 1;
    TeleprompterFile teleprompterFileForDownload;
    String From;
    boolean isFromRecordThisVideoButton=false;
    boolean isEditMode=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_telepromter);

        initViews();
        From = getIntent().getStringExtra("From");

        if (From!=null){
            prepareUIforRecordThisVideo();
            isFromRecordThisVideoButton=true;
        }

        {
            RecyclerView.LayoutManager layoutManager;
            layoutManager = new LinearLayoutManager(this);
            rvMyScripts.setLayoutManager(layoutManager);
            mAdapter = new TeleprompterFilesAdapter(new ArrayList<>(), this);
            rvMyScripts.setAdapter(mAdapter);
            getLoaderManager().initLoader(0, null, NewTeleprompter.this);
        }


    }

    private void prepareUIforRecordThisVideo() {
        String title=getIntent().getStringExtra("Title");
        String description=getIntent().getStringExtra("Description");
        String uri=getIntent().getStringExtra("URI");
        teleprompterFileForDownload=new TeleprompterFile();
        teleprompterFileForDownload.setTitle(title);
        teleprompterFileForDownload.setContent(description);

        setTabsBeahviour(false,true,false);
        isEditMode=true;
        rvMyScripts.setVisibility(View.GONE);
        llWriteNewScript.setVisibility(View.GONE);
        etTitle.setText(title);
        etDetail.setText(description);
//        llmyScripts.setEnabled(false);
        ivSave.setImageResource(R.drawable.ic_file_download_black_24dp);
        tvSave.setText("Download Script");
        viewEditScripts.setVisibility(View.VISIBLE);
//        rvRecordScripts.setVisibility(View.GONE);
//        rvMyScripts.setVisibility(View.GONE);



    }

    private void initViews() {
        llmyScripts=findViewById(R.id.newTeleprompter_llMyScripts);
        llmyScripts.setOnClickListener(this);
        llRecord=findViewById(R.id.newTeleprompter_llRecord);
        llRecord.setOnClickListener(this);
        llSave=findViewById(R.id.newTeleprompter_llSave);
        llSave.setOnClickListener(this);

        ivMyScripts=findViewById(R.id.newTeleprompter_ivMyScripts);
        ivRecord=findViewById(R.id.newTeleprompter_ivRecord);
        ivSave=findViewById(R.id.newTeleprompter_ivSave);
        tvMyScripts=findViewById(R.id.newTeleprompter_tvMyScripts);
        tvRecord=findViewById(R.id.newTeleprompter_tvRecord);
        tvSave=findViewById(R.id.newTeleprompter_tvSave);

        viewEditScripts=findViewById(R.id.newTeleprompter_viewEditScripts);
        viewEditScripts.setVisibility(View.GONE);

        rvMyScripts=findViewById(R.id.newTeleprompter_rvMyScripts);
        rvRecordScripts=findViewById(R.id.newTeleprompter_rvRecordScripts);

        etTitle=findViewById(R.id.editScripts_etTitle);
        etDetail=findViewById(R.id.editScripts_etWriteSomething);
        btnSaveScript=findViewById(R.id.editScripts_btnSave);
        btnSaveScript.setOnClickListener(this);

        llWriteNewScript=findViewById(R.id.newTeleprompter_llWriteScript);
        llWriteNewScript.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.newTeleprompter_llMyScripts:
                setTabsBeahviour(true,false,false);
                break;
            case R.id.newTeleprompter_llRecord:
                if (isEditMode) {
                    teleprompterFileForDownload.setContent(etDetail.getText().toString().trim());
                    isFromRecordButton = true;
                    btnSaveScript.performClick();
                    Intent intent = new Intent(this, VideoRecord.class);
                    intent.putExtra(EXTRA_FILE_DATA, teleprompterFileForDownload);
                    intent.putExtra("description", teleprompterFileForDownload.getContent());
                    startActivity(intent);
                }else
                    setTabsBeahviour(false,true,false);
                break;
            case R.id.newTeleprompter_llSave:
                if (isFileReadyToDownload) {
                    teleprompterFileForDownload.setContent(etDetail.getText().toString().trim());
                    downloadScript(teleprompterFileForDownload);
                } else if (isFromRecordThisVideoButton) {
                    teleprompterFileForDownload.setContent(etDetail.getText().toString().trim());
                    downloadScript(teleprompterFileForDownload);
                }
                break;
            case R.id.editScripts_btnSave:
                if (validateScriptFields()) {
                    saveDataToDb();
                }
                break;
            case R.id.newTeleprompter_llWriteScript:
                viewEditScripts.setVisibility(View.VISIBLE);
                rvMyScripts.setVisibility(View.GONE);
                rvRecordScripts.setVisibility(View.GONE);
                llWriteNewScript.setVisibility(View.GONE);
                mCurrentUri = null;
                isFileReadyToDownload = false;
                etDetail.setText("");
                etTitle.setText("");
                break;
        }

    }


    private void setTabsBeahviour(boolean isMyscript, boolean isRecord, boolean isSave){

        if (isMyscript){
            ivMyScripts.setImageResource(R.drawable.my_script_black);
            tvMyScripts.setTextColor(getResources().getColor(R.color.grey_900));
            ivRecord.setImageResource(R.drawable.record_gray);
            tvRecord.setTextColor(getResources().getColor(R.color.grey_40));
            ivSave.setImageResource(R.drawable.save_gray);
            tvSave.setTextColor(getResources().getColor(R.color.grey_40));
            isRecordClick=false;
//            viewEditScripts.setVisibility(View.VISIBLE);
//            rvMyScripts.setVisibility(View.GONE);
//            rvRecordScripts.setVisibility(View.GONE);
            tvSave.setText("Save");
            isFileReadyToDownload =false;
            llWriteNewScript.setVisibility(View.VISIBLE);
            viewEditScripts.setVisibility(View.GONE);
            rvMyScripts.setVisibility(View.VISIBLE);
            isEditMode=false;

//            rvMyScripts.setVisibility(View.VISIBLE);
//            rvRecordScripts.setVisibility(View.GONE);


        }else if (isRecord){
            ivMyScripts.setImageResource(R.drawable.my_scripts_gray);
            tvMyScripts.setTextColor(getResources().getColor(R.color.grey_40));
            ivRecord.setImageResource(R.drawable.record_black);
            tvRecord.setTextColor(getResources().getColor(R.color.grey_900));
            ivSave.setImageResource(R.drawable.save_gray);
            tvSave.setTextColor(getResources().getColor(R.color.grey_40));
            isRecordClick=true;
            isFileReadyToDownload =false;
            tvSave.setText("Save");
            llWriteNewScript.setVisibility(View.GONE);
            viewEditScripts.setVisibility(View.GONE);
            rvMyScripts.setVisibility(View.VISIBLE);


//            rvMyScripts.setVisibility(View.GONE);
//            rvRecordScripts.setVisibility(View.VISIBLE);
        }else if (isSave){
            ivMyScripts.setImageResource(R.drawable.my_scripts_gray);
            tvMyScripts.setTextColor(getResources().getColor(R.color.grey_40));
            ivRecord.setImageResource(R.drawable.record_gray);
            tvRecord.setTextColor(getResources().getColor(R.color.grey_40));
            ivSave.setImageResource(R.drawable.save_black);
            tvSave.setTextColor(getResources().getColor(R.color.grey_900));
            tvSave.setText("Save");
            isRecordClick=false;
            llWriteNewScript.setVisibility(View.GONE);
            viewEditScripts.setVisibility(View.GONE);
            llSave.setEnabled(false);

//            rvMyScripts.setVisibility(View.GONE);
//            rvRecordScripts.setVisibility(View.GONE);
        }else {

        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                TeleprompterFileContract.TeleprompterFileEvent._ID,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_TITLE,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_CONTENT,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_IMAGE,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_IS_FAV,
        };

        return new CursorLoader(this,
                TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI,
                projection,
                null,
                null,
                null);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

//        if (!isFromRecordThisVideoButton)
        {
            if (isMyScriptFileClick){

                if (cursor == null || cursor.getCount() < 1) {
                    return;
                }
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // The Cursor is now set to the right position
                    int idColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent._ID);
                    int titleColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_TITLE);
                    int contentColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_CONTENT);

                    // Extract out the value from the Cursor for the given column index
                    String id = cursor.getString(idColumnIndex);
                    String title = cursor.getString(titleColumnIndex);
                    String content = cursor.getString(contentColumnIndex);

//            etTitle.setText(title);
//            etDescription.setText(content);
//            setImage();
                }

            }else {

                if (cursor == null || cursor.getCount() < 1) {
//            tvEmpty.setVisibility(View.VISIBLE);
//            progressBar.setVisibility(View.INVISIBLE);
                    rvMyScripts.setVisibility(View.GONE);
                    return;
                }
//        tvEmpty.setVisibility(View.GONE);
//            rvMyScripts.setVisibility(View.VISIBLE);
                teleprompterFileList = new ArrayList<>();
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // The Cursor is now set to the right position
                    int idColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent._ID);
                    int titleColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_TITLE);
                    int contentColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_CONTENT);

                    // Extract out the value from the Cursor for the given column index
                    String id = cursor.getString(idColumnIndex);
                    String title = cursor.getString(titleColumnIndex);
                    String content = cursor.getString(contentColumnIndex);

                    TeleprompterFile teleprompterFile = new TeleprompterFile();
                    teleprompterFile.setId(id);
                    teleprompterFile.setTitle(title);
                    teleprompterFile.setContent(content);
                    teleprompterFileList.add(teleprompterFile);
                }
//        progressBar.setVisibility(View.INVISIBLE);
                mAdapter.setFileData(teleprompterFileList);

//        getInitialItemForWidget(teleprompterFileList);

            }
        }




    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        etTitle.setText("");
        etDetail.setText("");
    }

    @Override
    public void onDeleteComplete(int token, Object cookie, int result) {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFileClick(TeleprompterFile teleprompterFile) {


        if (isRecordClick) {
            Intent intent = new Intent(this, VideoRecord.class);
            intent.putExtra(EXTRA_FILE_DATA, teleprompterFile);
            intent.putExtra("description", teleprompterFile.getContent());
            startActivity(intent);
        }else {
            isMyScriptFileClick=true;
            viewEditScripts.setVisibility(View.VISIBLE);
            rvMyScripts.setVisibility(View.GONE);
            rvRecordScripts.setVisibility(View.GONE);
            llWriteNewScript.setVisibility(View.GONE);
            if (teleprompterFile!=null) {
                etTitle.setText(teleprompterFile.getTitle());
                etDetail.setText(teleprompterFile.getContent());
            }
            Intent intent = new Intent(NewTeleprompter.this, EditScripts.class);
            mCurrentUri= ContentUris.withAppendedId(TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI, Long.parseLong(teleprompterFile.getId()));
            teleprompterFileForDownload=teleprompterFile;
            ivSave.setImageResource(R.drawable.ic_file_download_black_24dp);
            tvSave.setText("Download Script");
            isFileReadyToDownload = true;
            isEditMode=true;
//            mCurrentUri=null;
//            intent.setData(currentItemUri);
//            intent.putExtra("Data",teleprompterFile);
//            currentItemUri=null;
//            startActivity(intent);
        }
    }

    private void downloadScript(TeleprompterFile teleprompterFile){
        Log.v(TAG, "onFileClick() " + teleprompterFile.toString());
        mDownloadedFile = teleprompterFile;
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // If you do not have permission, request it
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
        } else {
            // Download file

            new DownloadAsyncTask(this, teleprompterFile).execute();

        }
    }

    private void saveDataToDb() {
        final String title = etTitle.getText().toString();
        String content = etDetail.getText().toString();

        QueryHandler queryHandler = new QueryHandler(getContentResolver(), NewTeleprompter.this);

        ContentValues contentValues = new ContentValues();
        contentValues.put(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_TITLE, title);
        contentValues.put(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_CONTENT, content);
        contentValues.put(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_IMAGE, android.R.drawable.ic_menu_camera);

        if (!isFileReadyToDownload) {
            queryHandler.startInsert(1, null, TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI, contentValues);
        } else {
            queryHandler.startUpdate(1, null, mCurrentUri, contentValues, null, null);
        }

//        if (teleprompterFile==null){
//            queryHandler.startInsert(1, null, TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI, contentValues);
//        }else {
//            queryHandler.startUpdate(1, null, mCurrentUri, contentValues, null, null);
//        }
    }

    @Override
    public void onDownloadClick(TeleprompterFile teleprompterFile) {

    }

    @Override
    public void onDeleteClick(int position, TeleprompterFile teleprompterFile) {
        showDeleteConfirmationDialog(position, teleprompterFile);
    }

    @Override
    public void onEditClick(TeleprompterFile teleprompterFile) {

    }


    @Override
    public void onInsertComplete(int token, Object cookie, Uri uri) {
        if (uri == null) {
            Toast.makeText(this, R.string.failed_to_save_data, Toast.LENGTH_SHORT).show();
        } else {
            if (mAdapter!=null) mAdapter.notifyDataSetChanged();
            if (!isFromRecordButton) {
                Toast.makeText(this, R.string.saved_successfully, Toast.LENGTH_SHORT).show();
                setTabsBeahviour(true,false,false);
                getLoaderManager().restartLoader(0,null,this);
                startActivity(new Intent(this, NewTeleprompter.class));
                finish();
            }
        }
        isFromRecordButton = false;
    }

    @Override
    public void onUpdateComplete(int token, Object cookie, int result) {
        if (result == 0) {
            Toast.makeText(this, R.string.update_failed, Toast.LENGTH_SHORT).show();
        } else {

            if (mAdapter!=null) mAdapter.notifyDataSetChanged();

            if (!isFromRecordButton) {
                Toast.makeText(this, R.string.update_successful, Toast.LENGTH_SHORT).show();
                getLoaderManager().restartLoader(0, null, NewTeleprompter.this);
                setTabsBeahviour(true,false,false);
                startActivity(new Intent(this, NewTeleprompter.class));
                finish();
            }
        }
        isFromRecordButton = false;
    }

    private void showDeleteConfirmationDialog(final int position, final TeleprompterFile teleprompterFile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure to want to delete ?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteItem(position, teleprompterFile);

                if (teleprompterFileList.size()<1){
                    finish();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteItem(int position, TeleprompterFile teleprompterFile) {
        QueryDeleteHandler queryDeleteHandler = new QueryDeleteHandler(getContentResolver(), NewTeleprompter.this);
        Uri uriToDelete = TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI.buildUpon().appendPath(teleprompterFile.getId()).build();
        queryDeleteHandler.startDelete(position, null, uriToDelete, null, null);



    }

    private boolean validateScriptFields(){
        if (etTitle.getText().toString().isEmpty()){
            Toast.makeText(this, "Title should'nt be empty", Toast.LENGTH_SHORT).show();
            return false;
        }else if(etDetail.getText().toString().isEmpty()){
            Toast.makeText(this, "Text should'nt be empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
