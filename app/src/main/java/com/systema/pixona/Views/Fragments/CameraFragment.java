package com.systema.pixona.Views.Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.graphics.Typeface;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.annotation.NonNull;
import androidx.legacy.app.FragmentCompat;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.systema.pixona.R;
import com.systema.pixona.Utils.AutoFitTextureView;
import com.systema.pixona.Utils.CameraUtils;
import com.systema.pixona.Utils.MirroredTextView;
import com.systema.pixona.Utils.MyScrollingTextView;
import com.systema.pixona.Utils.MyScrollingTextView.OnStopRecording;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class CameraFragment extends Fragment implements
        View.OnClickListener, OnStopRecording,
        FragmentCompat.OnRequestPermissionsResultCallback{

    private float size = 24f;
    private View view;
    private TextView tvCounter;
    private Handler customHandler;
    private boolean delayDone;
    private Runnable scroll = new scrollRunnable();
    private int scrollSpeed;
    private boolean scrollText;
    private int time;
    private int timer;
    private SeekBar seekBar;
    private ImageView ivZoomIn;
    private ImageView ivZoomOut;
    private ImageView ivBack;
    private TextView textViewToChange, tvSize, tvScrollableText;
    private ScrollView scrollView, textScrollerScrollView;
    public static String getScript = null;
    public static String getSpeechID = null;
    public static boolean mIsRecordingVideo = false;
    private View view1;
    private RelativeLayout relativeLayout;
    private MyScrollingTextView myScrollingTextView;
    private RelativeLayout.LayoutParams params;
    private int counter;
    private String spaces17 = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    private LinearLayout textTeleprompterLayout;

    public CameraFragment() {
        // Required empty public constructor
    }


    private void init() {

        ivZoomIn = view.findViewById(R.id.camera_ZoomIn);
        ivZoomOut = view.findViewById(R.id.camera_ZoomOut);
        ivBack = view.findViewById(R.id.camera_back);
        tvSize = view.findViewById(R.id.camera_tvSize);
        seekBar = view.findViewById(R.id.camera_SeekBar);
        scrollView = view.findViewById(R.id.ScrollerTeleprompter);

        tvCounter = view.findViewById(R.id.camera_tvCounter);
        textViewToChange = view.findViewById(R.id.TextTeleprompter);

        relativeLayout = view.findViewById(R.id.relativeLayoutTop);
        textTeleprompterLayout = view.findViewById(R.id.TextTeleprompterLayout);

    }


    //region Camera WORK  here.
    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();

    private static final String TAG = "Camera2VideoFragment";
    private static final int REQUEST_VIDEO_PERMISSIONS = 1;
    private static final String FRAGMENT_DIALOG = "dialog";

    private static final String[] VIDEO_PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }

    /**
     * An {@link AutoFitTextureView} for camera preview.
     */
    private AutoFitTextureView mTextureView;

    /**
     * Button to record video
     */
    private ImageView mButtonVideo;

    /**
     * A reference to the opened {@link android.hardware.camera2.CameraDevice}.
     */
    private CameraDevice mCameraDevice;

    /**
     * A reference to the current {@link android.hardware.camera2.CameraCaptureSession} for
     * preview.
     */
    private CameraCaptureSession mPreviewSession;

    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture,
                                              int width, int height) {
            openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture,
                                                int width, int height) {
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }

    };

    /**
     * The {@link android.util.Size} of camera preview.
     */
    private Size mPreviewSize;

    /**
     * The {@link android.util.Size} of video recording.
     */
    private Size mVideoSize;

    /**
     * MediaRecorder
     */
    private MediaRecorder mMediaRecorder;

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;

    /**
     * A {@link Semaphore} to prevent the app from exiting before closing the camera.
     */
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its status.
     */
    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            mCameraDevice = cameraDevice;
            startPreview();
            mCameraOpenCloseLock.release();
            if (null != mTextureView) {
                configureTransform(mTextureView.getWidth(), mTextureView.getHeight());
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
            Activity activity = getActivity();
            if (null != activity) {
                activity.finish();
            }
        }

    };
    private Integer mSensorOrientation;
    private String mNextVideoAbsolutePath;
    private CaptureRequest.Builder mPreviewBuilder;

    public static CameraFragment newInstance() {
        return new CameraFragment();
    }

    /**
     * In this sample, we choose a video size with 3x4 aspect ratio. Also, we don't use sizes
     * larger than 1080p, since MediaRecorder cannot handle such a high-resolution video.
     *
     * @param choices The list of available sizes
     * @return The video size
     */

    private static Size chooseVideoSize(Size[] choices) {
        for (Size size : choices) {
            // Note that it will pick only HD video size, you should createVideoDetailsJSON more robust solution depending on screen size and available video sizes
            if (1920 == size.getWidth() && 1080 == size.getHeight()) {
                return size;
            }
        }
        Log.e(TAG, "Couldn't find any suitable video size");
        return choices[0];
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, chooses the smallest one whose
     * width and height are at least as large as the respective requested values, and whose aspect
     * ratio matches with the specified value.
     *
     * @param choices     The list of sizes that the camera supports for the intended output class
     * @param width       The minimum desired width
     * @param height      The minimum desired height
     * @param aspectRatio The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private static Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<Size>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        double ratio = (double) h / w;
        for (Size option : choices) {
            double optionRatio = (double) option.getHeight() / option.getWidth();
            if (ratio == optionRatio) {
                bigEnough.add(option);
            }
        }

        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            return Collections.max(bigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    //endregion

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView() called with: inflater = [" + inflater + "], container = [" + container + "], savedInstanceState = [" + savedInstanceState + "]");
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_camera, container, false);

        init();

        String description = getArguments().getString("description");
        description = description + spaces17;
        getScript = description;
        getSpeechID = "1";

        scrollSpeed = 21;
        MirroredTextView.mirror = false;
        textViewToChange.setLineSpacing((float) (10), 1.0f);
        textViewToChange.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto.ttf"), Typeface.NORMAL);
        textViewToChange.setTextSize(size);
        textViewToChange.setText(description);
        textViewToChange.setTextColor(Color.parseColor("#FFFFFF"));
        textViewToChange.setBackgroundColor(Color.parseColor("#700A0A0A"));

        time = scrollSpeed * 100;
        scrollText = false;
        delayDone = true;

        scrollView.setBackgroundColor(Color.parseColor("#700A0A0A"));
        scrollView.getChildAt(0).setOnClickListener(new clicked());

        customHandler = new Handler();

        ivZoomIn.setOnClickListener(v -> {
            if (size < 36f) {
                size = size + 3f;
            }
            textViewToChange.setTextSize(TypedValue.COMPLEX_UNIT_SP, size );
            textViewToChange.setText(textViewToChange.getText());
            if (tvScrollableText != null) {
                tvScrollableText.setTextSize(TypedValue.COMPLEX_UNIT_SP, size );
                tvScrollableText.setText(textViewToChange.getText());
            }
        });

        ivZoomOut.setOnClickListener(v -> {
            if (size > 12f) {
                size = size - 3f;
            }
            textViewToChange.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
            textViewToChange.setText(textViewToChange.getText());
            if (tvScrollableText != null) {
                tvScrollableText.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
                tvScrollableText.setText(textViewToChange.getText());
            }

        });

        int max = 50;
        int min = 1;
        seekBar.setMax(max - min);
        seekBar.setProgress(scrollSpeed);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)  {
                scrollSpeed = max - seekBar.getProgress();
                time = scrollSpeed * 100;
            }
        });


        ivBack.setOnClickListener(v -> {
            relativeLayout.removeView(view1);

            try{
                mMediaRecorder.stop();
            }catch (RuntimeException e) {
                Log.d(TAG, "RuntimeException: stop() is called immediately after start()");
            }

            if (mMediaRecorder != null) {
                mMediaRecorder.reset();
                mMediaRecorder.release();
                mMediaRecorder = null;
            }

            mIsRecordingVideo = false;
            mButtonVideo.setImageResource(R.drawable.btn_record);


            getActivity().finish();
        });

        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        mTextureView =  view.findViewById(R.id.texture);
        mButtonVideo = view.findViewById(R.id.video);
        mButtonVideo.setOnClickListener(this);
        view.findViewById(R.id.info).setOnClickListener(this);
        Log.d(TAG, "onViewCreated() called with: view = [" + view + "], savedInstanceState = [" + savedInstanceState + "]");


    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
        startBackgroundThread();
        if (mTextureView.isAvailable()) {
            openCamera(mTextureView.getWidth(), mTextureView.getHeight());
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause() called");
        closeCamera();
        stopBackgroundThread();
        if (this.customHandler != null) {
            this.customHandler.removeCallbacksAndMessages(null);
        }
        this.scrollText = false;


        try{
            mMediaRecorder.stop();
        }catch (RuntimeException e) {
            Log.d(TAG, "RuntimeException: stop() is called immediately after start()");
        }

        if (mMediaRecorder != null) {
            mMediaRecorder.reset();
            mMediaRecorder.release();
            mMediaRecorder = null;
        }

        mIsRecordingVideo = false;
        mButtonVideo.setImageResource(R.drawable.btn_record);


        super.onPause();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video: {
                if (!mIsRecordingVideo) {

                    counter = 2;

                    new CountDownTimer(2000, 1000){
                        public void onTick(long millisUntilFinished){
                            tvCounter.setVisibility(View.VISIBLE);
                            textViewToChange.setVisibility(View.GONE);
                            tvCounter.setText(String.valueOf(counter));
                            counter--;
                        }
                        public  void onFinish(){
                            tvCounter.setVisibility(View.GONE);
                            textViewToChange.setVisibility(View.VISIBLE);
                            startRecordingVideo();
                            tap();
                            if (getActivity()!=null)
                                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
                            Log.d(TAG, "onClick() if video called with:  view = [" + view +  "]");
                        }
                    }.start();

                    //                    view1 = LayoutInflater.from(getActivity()).inflate(R.layout.text_scroller, null);
//                    textScrollerScrollView = view1.findViewById(R.id.textScroller_scrollView);
//                    tvScrollableText = view1.findViewById(R.id.textScroller_textView);
//                    params = new RelativeLayout.LayoutParams(-2, -1);
//
//                    textScrollerScrollView.setBackgroundColor(Color.parseColor("#700A0A0A"));
//                    textScrollerScrollView.getChildAt(0).setOnClickListener(new clicked());
//
//                    tvScrollableText.setLineSpacing((float) (10), 1.0f);
//                    tvScrollableText.setTextSize(size);
//                    tvScrollableText.setTextColor(Color.parseColor("#FFFFFF"));
//                    tvScrollableText.setBackgroundColor(Color.parseColor("#700A0A0A"));
//
//                    time = scrollSpeed * 100;
//                    scrollText = false;
//                    delayDone = true;
//
//                    tvScrollableText.setText(getScript);
//
//                    params.addRule(2, textScrollerScrollView.getId());
//                    relativeLayout.addView(view1, params);
//
//                    customHandler = new Handler();

//                    tap();

                    //                    view1 = LayoutInflater.from(getActivity()).inflate(R.layout.text_scroller, null);
//                    myScrollingTextView = view1.findViewById(R.id.tvContent);
//                    params = new RelativeLayout.LayoutParams(-2, -1);
//
//                    myScrollingTextView.setText(getScript);
//                    myScrollingTextView.setSpeed((float) scrollSpeed);
//                    myScrollingTextView.setTextSize(size);
//                    myScrollingTextView.setMarqueeRepeatLimit(99);
//                    myScrollingTextView.setSelected(true);
//
//                    myScrollingTextView.setTextColor(Color.parseColor("#FFFFFF"));
//                    myScrollingTextView.setBackgroundColor(Color.parseColor("#700A0A0A"));
//
//                    params.addRule(2, mButtonVideo.getId());
//                    relativeLayout.addView(view1, params);

//                    seekBar.setVisibility(View.INVISIBLE);
//                    ivZoomIn.setVisibility(View.INVISIBLE);
//                    ivZoomOut.setVisibility(View.INVISIBLE);
//                    ivBack.setVisibility(View.INVISIBLE);
//                    tvSize.setVisibility(View.INVISIBLE);
//
//
//                    scrollView.setVisibility(View.GONE);
//                    textViewToChange.setVisibility(View.GONE);

                } else {

                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

                    //                    scrollView.setVisibility(View.VISIBLE);
//                    textViewToChange.setVisibility(View.VISIBLE);
//
//                    relativeLayout.removeView(view1);

//                    seekBar.setVisibility(View.VISIBLE);
//                    ivZoomIn.setVisibility(View.VISIBLE);
//                    ivZoomOut.setVisibility(View.VISIBLE);
//                    ivBack.setVisibility(View.VISIBLE);
//                    tvSize.setVisibility(View.VISIBLE);

                    tap();
                    stopRecordingVideo();
//                    closeCamera();
//                    closePreviewSession();
//                    getActivity().finish();

                    Log.d(TAG, "onClick() else video called with: view = [" + view + "]");

                }
                break;
            }
            case R.id.info: {
                Activity activity = getActivity();
                if (null != activity) {
                    new AlertDialog.Builder(activity)
                            .setMessage("some info")
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
                break;
            }
        }
    }


    //region More camera work here
    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        Log.d(TAG, "startBackgroundThread() called");
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        Log.d(TAG, "stopBackgroundThread() called");
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets whether you should show UI with rationale for requesting permissions.
     *
     * @return Whether you can show permission rationale UI.
     */
    private boolean shouldShowRequestPermissionRationale() {
        Log.d(TAG, "shouldShowRequestPermissionRationale() called");
        for (String permission : CameraFragment.VIDEO_PERMISSIONS) {
            if (FragmentCompat.shouldShowRequestPermissionRationale(this, permission)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Requests permissions needed for recording video.
     */
    private void requestVideoPermissions() {
        Log.d(TAG, "requestVideoPermissions() called");
        if (shouldShowRequestPermissionRationale()) {
            new ConfirmationDialog().show(getChildFragmentManager(), FRAGMENT_DIALOG);
        } else {
            FragmentCompat.requestPermissions(this, VIDEO_PERMISSIONS, REQUEST_VIDEO_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult");
        if (requestCode == REQUEST_VIDEO_PERMISSIONS) {
            if (grantResults.length == VIDEO_PERMISSIONS.length) {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        ErrorDialog.newInstance(getString(R.string.permission_request))
                                .show(getChildFragmentManager(), FRAGMENT_DIALOG);
                        break;
                    }
                }
            } else {
                ErrorDialog.newInstance(getString(R.string.permission_request))
                        .show(getChildFragmentManager(), FRAGMENT_DIALOG);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean hasPermissionsGranted(String[] permissions) {
        Log.d(TAG, "hasPermissionsGranted() called with: permissions = [" + permissions + "]");
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(getActivity(), permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    /**
     * Tries to open a {@link CameraDevice}. The result is listened by `mStateCallback`.
     */
    @SuppressWarnings("MissingPermission")
    private void openCamera(int width, int height) {
        screenRotationAngle();
        Log.d(TAG, "openCamera() called with: width = [" + width + "], height = [" + height + "]");
        if (!hasPermissionsGranted(VIDEO_PERMISSIONS)) {
            requestVideoPermissions();
            return;
        }
        final Activity activity = getActivity();
        if (null == activity || activity.isFinishing()) {
            return;
        }
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            Log.d(TAG, "tryAcquire");
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            String cameraId = manager.getCameraIdList()[1];

            // Choose the sizes for camera preview and video recording
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics
                    .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            if (map == null) {
                throw new RuntimeException("Cannot get available preview/video sizes");
            }
            mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder.class));
            mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                    width, height, mVideoSize);

            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                mTextureView.setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            } else {
                mTextureView.setAspectRatio(mPreviewSize.getHeight(), mPreviewSize.getWidth());
            }
            configureTransform(width, height);
            mMediaRecorder = new MediaRecorder();
            manager.openCamera(cameraId, mStateCallback, null);
        } catch (CameraAccessException e) {
            Toast.makeText(activity, "Cannot access the camera.", Toast.LENGTH_SHORT).show();
            activity.finish();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            ErrorDialog.newInstance(getString(R.string.camera_error))
                    .show(getChildFragmentManager(), FRAGMENT_DIALOG);
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.");
        }
    }

    private void closeCamera() {
        Log.d(TAG, "closeCamera() called");
        try {
            mCameraOpenCloseLock.acquire();
            closePreviewSession();
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mMediaRecorder) {
                mMediaRecorder.release();
                mMediaRecorder = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.");
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * Start the camera preview.
     */
    private void startPreview() {
        Log.d(TAG, "startPreview() called");
        if (null == mCameraDevice || !mTextureView.isAvailable() || null == mPreviewSize) {
            return;
        }
        try {

            closePreviewSession();
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

            Surface previewSurface = new Surface(texture);
            mPreviewBuilder.addTarget(previewSurface);

            mCameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mPreviewSession = session;
                            updatePreview();
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            Activity activity = getActivity();
                            if (null != activity) {
                                Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update the camera preview. {@link #startPreview()} needs to be called in advance.
     */
    private void updatePreview() {
        Log.d(TAG, "updatePreview() called");
        if (null == mCameraDevice) {
            return;
        }
        try {
            setUpCaptureRequestBuilder(mPreviewBuilder);
            HandlerThread thread = new HandlerThread("CameraPreview");
            thread.start();
            mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setUpCaptureRequestBuilder(CaptureRequest.Builder builder) {
        Log.d(TAG, "setUpCaptureRequestBuilder() called with: builder = [" + builder + "]");
        builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
    }

    /**
     * Configures the necessary {@link android.graphics.Matrix} transformation to `mTextureView`.
     * This method should not to be called until the camera preview size is determined in
     * openCamera, or until the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {
        Log.d(TAG, "configureTransform() called with: viewWidth = [" + viewWidth + "], viewHeight = [" + viewHeight + "]");
        Activity activity = getActivity();
        if (null == mTextureView || null == mPreviewSize || null == activity) {
            return;
        }
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        mTextureView.setTransform(matrix);
    }

    private void setUpMediaRecorder() {
        Log.d(TAG, "setUpMediaRecorder() called");
        final Activity activity = getActivity();

        if (null == activity) {
            return;
        }
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setVideoSize(mVideoSize.getWidth(), mVideoSize.getHeight());

        if (mNextVideoAbsolutePath == null || mNextVideoAbsolutePath.isEmpty())
            mNextVideoAbsolutePath = getVideoFilePath(getActivity());

        mMediaRecorder.setOutputFile(mNextVideoAbsolutePath);
        mMediaRecorder.setVideoEncodingBitRate(21982000);
//        mMediaRecorder.setVideoEncodingBitRate(2000 * 512);
        mMediaRecorder.setVideoFrameRate(9);
        //TODO
//        mMediaRecorder.setVideoSize(mVideoSize.getWidth(), mVideoSize.getHeight());
//
        final int bitDepth = 16;
        final int sampleRate ;//= 48000;
        final int bitRate = 192000000;

        AudioManager audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        String rate = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
        sampleRate = rate != null && !rate.isEmpty()? Integer.parseInt(rate): 0;
        //String size = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER);

        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mMediaRecorder.setAudioEncodingBitRate(bitRate);
        if (sampleRate > 0) {
            mMediaRecorder.setAudioSamplingRate(sampleRate);
        }
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        switch (mSensorOrientation) {
            case SENSOR_ORIENTATION_DEFAULT_DEGREES:
                mMediaRecorder.setOrientationHint(DEFAULT_ORIENTATIONS.get(rotation));
                break;
            case SENSOR_ORIENTATION_INVERSE_DEGREES:
                mMediaRecorder.setOrientationHint(INVERSE_ORIENTATIONS.get(rotation));
                break;
        }
//        mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        try {
            mMediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "setUpMediaRecorder() called IO Exception :"+e.toString());
        }
    }

    private String getVideoFilePath(Context context) {
        Log.d(TAG, "getVideoFilePath() called with: context = [" + context + "]");

        File  dir= null;
        try {
            dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Xunami");
            if (!dir.exists())
            {
                dir.mkdirs();
            }
//        final File dir = context.getExternalFilesDir(null);
            Log.d(TAG, "getVideoFilePath: dir="+dir.getAbsolutePath() );
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "getVideoFilePath() called with: Exception = [" + e.toString() + "]");
        }
//        final File dir = context.getExternalFilesDir(Environment.DIRECTORY_MOVIES);
        Log.d(TAG, "getVideoFilePath() called with: dir = [" + dir.getAbsolutePath() + "]");
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + System.currentTimeMillis() + ".mp4";

    }

    private void startRecordingVideo() {
        Log.d(TAG, "startRecordingVideo() called");
        if (null == mCameraDevice || !mTextureView.isAvailable() || null == mPreviewSize) {
            return;
        }
        try {
            closePreviewSession();

            setUpMediaRecorder();

            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            List<Surface> surfaces = new ArrayList<>();

            // Set up Surface for the camera preview
            Surface previewSurface = new Surface(texture);
            surfaces.add(previewSurface);
            mPreviewBuilder.addTarget(previewSurface);

            // Set up Surface for the MediaRecorder
            Surface recorderSurface = mMediaRecorder.getSurface();
            surfaces.add(recorderSurface);
            mPreviewBuilder.addTarget(recorderSurface);

            // Start a capture session
            // Once the session starts, we can update the UI and start recording
            mCameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {

                    mPreviewSession = cameraCaptureSession;

                    updatePreview();

                    getActivity().runOnUiThread(() -> {
                        // UI
                        //TODO button
                        mButtonVideo.setImageResource(R.drawable.bg_record);
//                            mButtonVideo.setText(R.string.stop);
                        mIsRecordingVideo = true;
                        // Start recording
                        mMediaRecorder.start();
                    });
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Activity activity = getActivity();
                    if (null != activity) {
                        Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }, mBackgroundHandler);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "startRecordingVideo() called with Exception :"+e.toString());
        }

    }

    private void closePreviewSession() {
        Log.d(TAG, "closePreviewSession() called");
        if (mPreviewSession != null) {
            mPreviewSession.close();
            mPreviewSession = null;
        }
    }

    private void stopRecordingVideo() {
        Log.d(TAG, "stopRecordingVideo() called");

        try{
            mMediaRecorder.stop();
            mMediaRecorder.reset();
        }catch (RuntimeException e) {
            Log.d(TAG, "RuntimeException: stop() is called immediately after start()");
        }


//        mMediaRecorder.release();

        mIsRecordingVideo = false;
        mButtonVideo.setImageResource(R.drawable.btn_record);


        CameraUtils.refreshGallery(getActivity(), mNextVideoAbsolutePath);
        String desPath=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();

        Log.e(TAG, "stopRecordingVideo: SourcePath :"+mNextVideoAbsolutePath);
        Log.d(TAG, "stopRecordingVideo: Destpath"+ desPath+"/"+ System.currentTimeMillis() + ".mp4");

        Activity activity = getActivity();
        if (null != activity) {
            Toast.makeText(activity, "Video saved: " + mNextVideoAbsolutePath, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Video saved: " + mNextVideoAbsolutePath);
        }
        mNextVideoAbsolutePath = null;
        startPreview();

    }

    @Override
    public void OnStopRecording(String str, String str2) {

    }


    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            Log.d(TAG, "compare() called with: lhs = [" + lhs + "], rhs = [" + rhs + "]");
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    public static class ErrorDialog extends DialogFragment {

        private static final String ARG_MESSAGE = "message";

        public static ErrorDialog newInstance(String message) {
            Log.d(TAG, " Error Dialog newInstance() called with: message = [" + message + "]");
            ErrorDialog dialog = new ErrorDialog();
            Bundle args = new Bundle();
            args.putString(ARG_MESSAGE, message);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Log.d(TAG, "onCreateDialog() called with: savedInstanceState = [" + savedInstanceState + "]");
            final Activity activity = getActivity();
            return new AlertDialog.Builder(activity)
                    .setMessage(getArguments().getString(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.finish();
                        }
                    })
                    .create();
        }

    }

    public static class ConfirmationDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Log.d(TAG, "onCreateDialog() called with: savedInstanceState = [" + savedInstanceState + "]");
            final android.app.Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.permission_request)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FragmentCompat.requestPermissions(parent, VIDEO_PERMISSIONS,
                                    REQUEST_VIDEO_PERMISSIONS);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    parent.getActivity().finish();
                                }
                            })
                    .create();
        }

    }

    //endregion


    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 1) {
            tap();
        }
        return true;
    }

    private void tap() {
        Log.d(TAG, "tap() called");
        if (delayDone) {
            scrollText = !scrollText;
            customHandler.post(scroll);

        }
    }

    class scrollRunnable implements Runnable {
        scrollRunnable() {}

        public void run() {
//            Log.d(TAG, "run() called");
            if (scrollText) {
                if (timer > 0) {
                    timer = timer - 1;
                } else {
                    if (textScrollerScrollView!=null)
                        textScrollerScrollView.scrollTo(0, textScrollerScrollView.getScrollY() + 1);
                    scrollView.scrollTo(0, scrollView.getScrollY() + 1);
                    timer = time;
                }
                customHandler.post(scroll);
            }
        }
    }

    class clicked implements View.OnClickListener {
        clicked() {
        }
        public void onClick(View view) {
            Log.d(TAG, "onClick() called with: view = [" + view + "]");
            tap();
        }
    }

    //    private class CountdownRunnable implements Runnable{
//
//
//        @Override
//        public void run() {
//            int count = Integer.parseInt(countDownText.getText().toString());
//            if(count>1){
//                count--;
//                countDownText.setText(Integer.toString(count));
//                new Handler().postDelayed(new CountdownRunnable(),1000);
//            }else {
//                countDownText.setText(getString(R.string.countdown_start));
//                hideCountDownView();
//                startScrollAnimation();
//            }
//
//
//        }
//    }

    private static final int ORIENTATION_0 = 0;
    private static final int ORIENTATION_90 = 3;
    private static final int ORIENTATION_270 = 1;
    private  void screenRotationAngle()
    {
        Display display = ((WindowManager)
                getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int screenOrientation = display.getRotation();

        switch (screenOrientation)
        {
            default:
            case ORIENTATION_0: // Portrait
                // do smth.
                Log.d(TAG, "screenRotationAngle: onConfigurationChanged Portrait");
                break;
            case ORIENTATION_90: // Landscape right
                // do smth.
                Log.d(TAG, "screenRotationAngle: onConfigurationChanged Landscape right");
                textTeleprompterLayout.setGravity(Gravity.RIGHT);

                break;
            case ORIENTATION_270: // Landscape left
                // do smth.
                Log.d(TAG, "screenRotationAngle: onConfigurationChanged Landscape left");
                textTeleprompterLayout.setGravity(Gravity.LEFT);
                break;
        }
    }
}
