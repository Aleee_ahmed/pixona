package com.systema.pixona.Views.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

public class UserDetails extends AppCompatActivity {

    private MKLoader loader;
    private Switch switchActive, switchPending, switchDeactive;
    private ImageView ivIndicator, ivBack;
    private TextView tvName, tvDate, tvEmail, tvPhone, tvAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        initComponents();

        ivBack.setOnClickListener(v -> finish());

        switchActive.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                if (updateDatabase(Constants.ACTIVE).get()) {
                    switchPending.setChecked(false);
                    switchDeactive.setChecked(false);
                    ivIndicator.setImageResource(R.drawable.dot_green);
                } else {
                    switchActive.setChecked(true);
                }

            } else {
                if (!switchPending.isChecked() && !switchDeactive.isChecked())
                    switchActive.setChecked(true);
            }
        });

        switchPending.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                if (updateDatabase(Constants.PENDING).get()) {
                    switchActive.setChecked(false);
                    switchDeactive.setChecked(false);
                    ivIndicator.setImageResource(R.drawable.dot_yellow);
                } else {
                    switchPending.setChecked(true);
                }

            } else {
                if (!switchActive.isChecked() && !switchDeactive.isChecked())
                    switchPending.setChecked(true);
            }
        });

        switchDeactive.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                if (updateDatabase(Constants.DEACTIVATE).get()) {
                    switchActive.setChecked(false);
                    switchPending.setChecked(false);
                    ivIndicator.setImageResource(R.drawable.dot_red);
                } else {
                    switchDeactive.setChecked(true);
                }

            } else {
                if (!switchActive.isChecked() && !switchPending.isChecked())
                    switchDeactive.setChecked(true);
            }
        });

    }

    private AtomicBoolean updateDatabase(String status) {
        AtomicBoolean flag = new AtomicBoolean(true);

        showProgress();

        FirebaseDatabase.getInstance().getReference("users")
                .child(Constants.encode(tvEmail.getText().toString()))
                .child("status")
                .setValue(status, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        Toast.makeText(this, String.format(Locale.ENGLISH, "%s's status has been changed to %s", tvName.getText().toString(), status), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                        flag.set(false);
                    }
                    hideProgress();

                });
        return flag;
    }

    private void showProgress() {
        loader.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        loader.setVisibility(View.GONE);
    }

    private void initComponents() {

        ivIndicator = findViewById(R.id.userDetails_ivIndicator);
        tvName = findViewById(R.id.userDetails_tvName);
        tvDate = findViewById(R.id.userDetails_tvDate);
        tvEmail = findViewById(R.id.userDetails_tvMail);
        tvPhone = findViewById(R.id.userDetails_tvPhone);
        tvAddress = findViewById(R.id.userDetails_tvAdress);
        ivBack = findViewById(R.id.userDetails_ivBack);
        switchActive = findViewById(R.id.userDetails_switchActive);
        switchPending = findViewById(R.id.userDetails_switchPending);
        switchDeactive = findViewById(R.id.userDetails_switchDeactive);
        loader = findViewById(R.id.userDetails_loader);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            tvName.setText(bundle.getString("name"));
            tvDate.setText(bundle.getString("date"));
            tvEmail.setText(bundle.getString("email"));
            tvPhone.setText(Constants.decode(bundle.getString("phone")));
            tvAddress.setText(bundle.getString("address"));
            ivIndicator.setImageResource(bundle.getInt("indicator"));
            String status = bundle.getString("status");
            if (status.equalsIgnoreCase(Constants.ACTIVE)) {
                switchActive.setChecked(true);
            } else if (status.equalsIgnoreCase(Constants.PENDING)) {
                switchPending.setChecked(true);
            } else {
                switchDeactive.setChecked(true);
            }
        }

    }
}
