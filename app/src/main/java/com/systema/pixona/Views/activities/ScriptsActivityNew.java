package com.systema.pixona.Views.activities;

import android.Manifest;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.systema.pixona.R;
import com.systema.pixona.Utils.DownloadAsyncTask;
import com.systema.pixona.Utils.QueryDeleteHandler;
import com.systema.pixona.Utils.TeleprompterFile;
import com.systema.pixona.Data.TeleprompterFileContract;

import java.util.ArrayList;
import java.util.List;

import static com.systema.pixona.Utils.Constants.EXTRA_FILE_DATA;


public class ScriptsActivityNew extends AppCompatActivity implements TeleprompterFilesAdapter.OnFileClickListener,
        QueryDeleteHandler.onQueryHandlerDeleteComplete,
        LoaderManager.LoaderCallbacks<Cursor>{

    private static final String TAG ="ScriptsActivityNew" ;
    private TeleprompterFilesAdapter mAdapter;
    private TeleprompterFile mDownloadedFile;
    private static final int REQUEST_STORAGE_PERMISSION = 1;
    private RecyclerView rvScripts;
    private FloatingActionButton fbNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scripts_new);
        rvScripts=findViewById(R.id.scriptsNew_rvScripts);
        fbNew=findViewById(R.id.scriptsNew_fbNew);

        fbNew.setOnClickListener(v -> startActivity(new Intent(ScriptsActivityNew.this, EditScripts.class)));

        RecyclerView.LayoutManager layoutManager;

        layoutManager = new LinearLayoutManager(this);

        rvScripts.setLayoutManager(layoutManager);

        mAdapter = new TeleprompterFilesAdapter(new ArrayList<>(), this);
        rvScripts.setAdapter(mAdapter);

        getLoaderManager().initLoader(0, null, ScriptsActivityNew.this);

    }

    @Override
    public void onFileClick(TeleprompterFile teleprompterFile) {
        startTeleprompterActivity(teleprompterFile);
    }

    @Override
    public void onDownloadClick(TeleprompterFile teleprompterFile) {
        Log.v(TAG, "onFileClick() " + teleprompterFile.toString());
        mDownloadedFile = teleprompterFile;
        // Check for the external storage permission
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // If you do not have permission, request it
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
        } else {
            // Download file
            DownloadAsyncTask downloadAsyncTask = new DownloadAsyncTask(this, teleprompterFile);
            downloadAsyncTask.execute();
        }
    }

    @Override
    public void onDeleteClick(int position, TeleprompterFile teleprompterFile) {
        Log.v(TAG, "onDeleteClick " + teleprompterFile.toString());
        showDeleteConfirmationDialog(position, teleprompterFile);
    }

    @Override
    public void onEditClick(TeleprompterFile teleprompterFile) {
        Log.v("ScriptsActivityNew", "onEditClick " + teleprompterFile.toString());
        openAddFileActivityWithExtras(teleprompterFile);

    }

    @Override
    public void onDeleteComplete(int token, Object cookie, int result) {
        Log.v(TAG, "onDeleteComplete result" + result);
        if (result == 0) {
            Toast.makeText(this, "Delete Failed", Toast.LENGTH_SHORT).show();
        } else {
            // Otherwise, the delete was successful and we can display a toast.
            Toast.makeText(this, "Delete Successfully", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String projection[] = {
                TeleprompterFileContract.TeleprompterFileEvent._ID,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_TITLE,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_CONTENT,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_IMAGE,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_IS_FAV,
        };
        return new CursorLoader(this,
                TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI,
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null || cursor.getCount() < 1) {
//            tvEmpty.setVisibility(View.VISIBLE);
//            progressBar.setVisibility(View.INVISIBLE);
            rvScripts.setVisibility(View.GONE);
            return;
        }
//        tvEmpty.setVisibility(View.GONE);
        rvScripts.setVisibility(View.VISIBLE);
        List<TeleprompterFile> teleprompterFileList = new ArrayList<>();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            // The Cursor is now set to the right position
            int idColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent._ID);
            int titleColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_TITLE);
            int contentColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_CONTENT);

            // Extract out the value from the Cursor for the given column index
            String id = cursor.getString(idColumnIndex);
            String title = cursor.getString(titleColumnIndex);
            String content = cursor.getString(contentColumnIndex);

            TeleprompterFile teleprompterFile = new TeleprompterFile();
            teleprompterFile.setId(id);
            teleprompterFile.setTitle(title);
            teleprompterFile.setContent(content);
            teleprompterFileList.add(teleprompterFile);
        }
//        progressBar.setVisibility(View.INVISIBLE);
        mAdapter.setFileData(teleprompterFileList);

//        getInitialItemForWidget(teleprompterFileList);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void openAddFileActivityWithExtras(TeleprompterFile teleprompterFile) {
        Intent intent = new Intent(ScriptsActivityNew.this, EditScripts.class);
        Uri currentItemUri = ContentUris.withAppendedId(TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI, Long.parseLong(teleprompterFile.getId()));
        intent.setData(currentItemUri);
        intent.putExtra("Data",teleprompterFile);
        currentItemUri=null;
        startActivity(intent);

    }

    private void showDeleteConfirmationDialog(final int position, final TeleprompterFile teleprompterFile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure to want to delete ?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteItem(position, teleprompterFile);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteItem(int position, TeleprompterFile teleprompterFile) {
        QueryDeleteHandler queryDeleteHandler = new QueryDeleteHandler(getContentResolver(), ScriptsActivityNew.this);
        Uri uriToDelete = TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI.buildUpon().appendPath(teleprompterFile.getId()).build();
        queryDeleteHandler.startDelete(position, null, uriToDelete, null, null);
    }

    private void startTeleprompterActivity(final TeleprompterFile teleprompterFile) {
        Intent intent = new Intent(this, VideoRecord.class);
        intent.putExtra(EXTRA_FILE_DATA, teleprompterFile);
        intent.putExtra("description", teleprompterFile.getContent());
        startActivity(intent);
    }
}
