package com.systema.pixona.Views.activities;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.systema.pixona.Network.PostAPIHandlerAsync;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.Internet;
import com.systema.pixona.Utils.Json;
import com.systema.pixona.Utils.SharedPreference;
import com.systema.pixona.Utils.SnackBarr;

public class Submit extends AppCompatActivity implements PostAPIHandlerAsync.PostAsyncResponse {

    private ProgressBar progressBar;
    private EditText etName,etNoFIle,etGuest,etNotes,etCompany, etTemplateVideoTitle;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);

        initComponents();

    }

    private void initComponents() {
        etName = findViewById(R.id.submit_etYourName);
        etNoFIle = findViewById(R.id.submit_etFile);
        etTemplateVideoTitle = findViewById(R.id.submit_etTemplateVideoTitle);
        etGuest = findViewById(R.id.submit_etInfo);
        etNotes = findViewById(R.id.submit_etNotes);
        etCompany = findViewById(R.id.submit_etCompanyName);

        etName.setText(SharedPreference.getLoginUserEmail(this));
        etName.setEnabled(false);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            etNoFIle.setText(bundle.getString("no_of_files"));
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationForOreo("Xunami", "Your video has been uploaded successfully.", 0, R.drawable.logo_50);
            } else {
                NotificationBelowOreo("Xunami", "Your video has been uploaded successfully.", 0, R.drawable.logo_50);
            }
            etNoFIle.setText(String.valueOf(Constants.NoOfFiles));
        }

        etNoFIle.setEnabled(false);

        progressBar = findViewById(R.id.pbSubmit);
        btnSubmit = findViewById(R.id.submit_btnSubmit);
    }

    public void SubmitOnClick(View view) {

        if (etName.getText().toString().isEmpty()) {
            SnackBarr.showErrorSnackBar(view, this, "Your name cannot be empty.");
            return;
        }
        if (etTemplateVideoTitle.getText().toString().isEmpty()) {
            SnackBarr.showErrorSnackBar(view, this, "Template name cannot be empty.");
            return;
        }
        if (etNoFIle.getText().toString().isEmpty()) {
            SnackBarr.showErrorSnackBar(view, this, "# of files cannot be empty.");
            return;
        }
        if (etGuest.getText().toString().isEmpty()) {
            SnackBarr.showErrorSnackBar(view, this, "Guest name cannot be empty.");
            return;
        }
        if (etNotes.getText().toString().isEmpty()) {
            SnackBarr.showErrorSnackBar(view, this, "Notes cannot be empty.");
            return;
        }
        if (etCompany.getText().toString().isEmpty()) {
            SnackBarr.showErrorSnackBar(view, this, "Company name cannot be empty.");
            return;
        }

        if (!Internet.isConnected(this)) {
            SnackBarr.showErrorSnackBar(progressBar, this, "No internet connection.");
            return;
        }

        showProgress();

        String json = Json.createVideoDetailsJSON(etName.getText().toString(),
                etNoFIle.getText().toString(),
                etTemplateVideoTitle.getText().toString(),
                etGuest.getText().toString(),
                etNotes.getText().toString(),
                etCompany.getText().toString());

        String url="https://api.smartsheet.com/2.0/sheets/6893456468535172/rows/";

        new PostAPIHandlerAsync(url, json, "POST", this).execute();


    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        btnSubmit.setEnabled(false);
    }
    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
        btnSubmit.setEnabled(true);
    }

    @Override
    public void postProcessFinished(String output) {

        if (output.contains("SUCCESS")) {
            Toast.makeText(this, "Data uploaded successfully.", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), Thanks.class));
            finish();
        } else if (output.contains("Exception")) {
            SnackBarr.showErrorSnackBar(progressBar, this, output);
        } else {
            SnackBarr.showErrorSnackBar(progressBar, this, "Something went wrong. Please submit data again.");
        }

        hideProgress();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void  NotificationForOreo(String Title, String msg, int id, int Icon) {

        String cid = "my_channel_01";

        CharSequence name ="Xunami Notification Channel";


        String description = "Xunami video submission app";

        int importance = NotificationManager.IMPORTANCE_HIGH;

        NotificationChannel mChannel = new NotificationChannel(cid, name, importance);

        mChannel.setDescription(description);

        mChannel.enableLights(true);

        mChannel.setLightColor(Color.RED);

        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

        Intent intent = new Intent(getApplicationContext(), Submit.class);
        intent.putExtra("no_of_files", Constants.NoOfFiles);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);


        Notification.Builder builder = new Notification.Builder(getApplicationContext())
                .setContentTitle(Title)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(Icon)
                .setNumber(0)
                .setDefaults(Notification.DEFAULT_ALL)
                .setChannelId(cid);


        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(mChannel);
        notificationManager.notify(id, notification);


    }

    public void NotificationBelowOreo(String Title, String msg, int id, int Icon) {


        Intent intent = new Intent(getApplicationContext(), Submit.class);
        intent.putExtra("no_of_files", Constants.NoOfFiles);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification.Builder builder = new Notification.Builder(getApplicationContext())
                .setContentTitle(Title)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(Icon)
                .setNumber(0)
                .setDefaults(Notification.DEFAULT_ALL);


        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify(id, notification);

    }


}
