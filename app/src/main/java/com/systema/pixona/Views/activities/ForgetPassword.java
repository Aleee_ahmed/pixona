package com.systema.pixona.Views.activities;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.systema.pixona.Interfaces.ISendEmail;
import com.systema.pixona.Presenter.SendEmailPresenter;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.Internet;
import com.tuyenmonkey.mkloader.MKLoader;

public class ForgetPassword extends AppCompatActivity
        implements ISendEmail.iSendEmailMain {

    private EditText etEmail;
    private MKLoader loader;
    private Button btnForgetPassword;
    private SendEmailPresenter presenter;

    private void initComponents() {
        presenter = new SendEmailPresenter(this);
        etEmail = findViewById(R.id.ForgetPasword_etEmail);
        btnForgetPassword = findViewById(R.id.forgetPassword_btnResetPassword);
        loader = findViewById(R.id.forgetPassword_loader);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        initComponents();

    }


    public void btnForgetPasswordOnClick(View view) {

        if (!Internet.isConnected(this)) {
            Toast.makeText(this, "No internet connectivity. Please check your internet connection.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!TextUtils.isEmpty(etEmail.getText())) {

            showProgress();

            FirebaseDatabase.getInstance().getReference("users")
                    .child(Constants.encode(etEmail.getText().toString().trim()))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue() != null) {

                                sendEmail(
                                        dataSnapshot.getKey(),
                                        dataSnapshot.child("password").getValue(String.class)
                                );


                            } else {
                                hideProgress();
                                Toast.makeText(ForgetPassword.this, "Email not registered.", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            hideProgress();
                            Toast.makeText(ForgetPassword.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

        } else {
            etEmail.setError("Please Enter a valid Email");
        }

    }


    private void sendEmail(String email, String password) {
        presenter.sendEmail(Constants.decode(email), "Xunami: Recover your password", "Password for your account is given below: " + password);
    }

    @Override
    public void showProgress() {
        etEmail.setEnabled(false);
        loader.setVisibility(View.VISIBLE);
        btnForgetPassword.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        etEmail.setEnabled(true);
        loader.setVisibility(View.GONE);
        btnForgetPassword.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayResult(String model) {
        if (model.contains("Thank you")) {
            Toast.makeText(this, "You password details have been sent to your email.", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, "Oops.. Something went wrong. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

}
