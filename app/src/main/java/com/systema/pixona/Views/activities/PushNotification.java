package com.systema.pixona.Views.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.firebase.database.FirebaseDatabase;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Internet;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.HashMap;

public class PushNotification extends AppCompatActivity {

    private EditText etWriteSomething;
    private Button btnSend;
    private MKLoader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_notification);
        initComponents();

        btnSend.setOnClickListener(v -> {
            if (Internet.isConnected(this)) {
                if (etWriteSomething.getText().toString().trim().isEmpty()) {
                    Toast.makeText(this, "Write something to send to all users.", Toast.LENGTH_SHORT).show();
                    return;
                }
                showProgress();
                addToNotificationsTable();
            } else {
                Toast.makeText(this, "Internet not connected.", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void hideProgress() {
        loader.setVisibility(View.GONE);
        btnSend.setVisibility(View.VISIBLE);
    }

    private void showProgress() {
        loader.setVisibility(View.VISIBLE);
        btnSend.setVisibility(View.GONE);
    }

    private void addToNotificationsTable() {

        HashMap<String, String> map = new HashMap<>();
        map.put("title", etWriteSomething.getText().toString().trim());

        FirebaseDatabase.getInstance().getReference("notifications")
                .child(String.valueOf(System.currentTimeMillis()))
                .setValue(map, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        Toast.makeText(this, "Notification is sent to all users.", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                });


    }

    private void initComponents() {
        etWriteSomething = findViewById(R.id.pushNotifications_etWriteSomething);
        btnSend = findViewById(R.id.pushNotifications_btnSend);
        loader = findViewById(R.id.pushNotifications_loader);
    }
}
