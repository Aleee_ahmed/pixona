package com.systema.pixona.Views.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.asksira.webviewsuite.WebViewSuite;
import com.systema.pixona.R;

public class WebView extends AppCompatActivity {

    private WebViewSuite webViewSuite ;

    private void init() {
        webViewSuite = findViewById(R.id.webView_webViewSuite);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        init();

        webViewSuite.startLoading("https://xunami.net/terms-of-service/");
        webViewSuite.setVisibility(View.VISIBLE);

    }

}
