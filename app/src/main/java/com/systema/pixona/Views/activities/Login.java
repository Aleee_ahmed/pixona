package com.systema.pixona.Views.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.Internet;
import com.systema.pixona.Utils.SharedPreference;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.Objects;

public class Login extends AppCompatActivity {
    private EditText etEmail,etPassword;
    private Button btnLogin;
    private MKLoader loader;
    private String email, password, firebaseToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (SharedPreference.getLoginStatus(Login.this)) {
            if (SharedPreference.getLoginUserEmail(this).equalsIgnoreCase(Constants.ADMIN))
                startActivity(new Intent(this, AdminMenu.class));
            else
                startActivity(new Intent(this, SelectFile.class));
            finish();
        }

        initComponents();

    }

    private void initComponents() {
        etEmail = findViewById(R.id.Login_etEmail);
        etPassword = findViewById(R.id.Login_etPassword);
        btnLogin = findViewById(R.id.Login_btnLogin);
        loader = findViewById(R.id.login_loader);
        FirebaseInstanceId
                .getInstance()
                .getInstanceId()
                .addOnSuccessListener(
                        instanceIdResult -> firebaseToken = instanceIdResult.getToken()
                );
    }

    private boolean validateFields(){


        if (TextUtils.isEmpty(etEmail.getText())){
            etEmail.setError("Email should'nt be empty");
            return false;
        }else etEmail.setError(null);

        if (TextUtils.isEmpty(etPassword.getText())){
            etPassword.setError("Password should'nt be empty");
            return false;
        }else etPassword.setError(null);

        return true;
    }

    private void showProgress() {
        loader.setVisibility(View.VISIBLE);
        btnLogin.setVisibility(View.GONE);
        etEmail.setEnabled(false);
        etPassword.setEnabled(false);
    }

    private void hideProgress() {
        loader.setVisibility(View.GONE);
        btnLogin.setVisibility(View.VISIBLE);
        etEmail.setEnabled(true);
        etPassword.setEnabled(true);
    }

    public void btnLoginClick(View view){

        if (Internet.isConnected(this)) {

            if (validateFields()) {

                email = Constants.encode(etEmail.getText().toString().toLowerCase().trim());
                password = etPassword.getText().toString();

                showProgress();

                checkEmailRegistered();

            }

        } else {
            Toast.makeText(this, "Internet not connected.", Toast.LENGTH_SHORT).show();
        }


    }

    private void checkEmailRegistered() {
        FirebaseDatabase.getInstance().getReference("users")
                .child(email)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getValue() != null) {

                            matchUserPassword(dataSnapshot);

                        } else {
                            hideProgress();
                            Toast.makeText(Login.this, "Email not registered.", Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        hideProgress();
                        Toast.makeText(Login.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void matchUserPassword(@NonNull DataSnapshot dataSnapshot) {
        if (Objects.requireNonNull(dataSnapshot.child("password").getValue(String.class)).equals(password)) {

            if (!Objects.requireNonNull(dataSnapshot.child("status").getValue(String.class))
                    .equalsIgnoreCase(Constants.DEACTIVATE)) {

                //Updating Firebase token..
                FirebaseDatabase.getInstance().getReference("users")
                        .child(email)
                        .child("device_id")
                        .setValue(firebaseToken, (databaseError, databaseReference) -> {

                            String name = dataSnapshot.child("name").getValue(String.class);
                            String password = dataSnapshot.child("password").getValue(String.class);

                            Toast.makeText(Login.this, "Logged In Successfully.", Toast.LENGTH_SHORT).show();

                            SharedPreference.save(Login.this, name, password, email);

                            if (email.equalsIgnoreCase(Constants.encode(Constants.ADMIN)))
                                startActivity(new Intent(Login.this, AdminMenu.class));
                            else
                                startActivity(new Intent(Login.this, SelectFile.class));
                            finish();

                            hideProgress();

                        });

            } else {
                hideProgress();
                Toast.makeText(this, "Your account has been deactivated. Please contact us if you have any questions.", Toast.LENGTH_LONG).show();
            }


        } else {
            hideProgress();
            Toast.makeText(Login.this, "You have entered a wrong password.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void tvForgetPasswordClick(View view){
        startActivity(new Intent(this, ForgetPassword.class));
    }

    public void tvSignUpClick(View view){
        startActivity(new Intent(this, Package.class));
    }
}
