package com.systema.pixona.Views.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.systema.pixona.Models.VideosModel;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Views.activities.VideoPreview;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Locale;

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ViewHolder> {

    private ArrayList<VideosModel> arrayList;

    public VideosAdapter(ArrayList<VideosModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_videos, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.tvVideoName.setText(arrayList.get(holder.getAdapterPosition()).getVideoTitle());
        String time = arrayList.get(holder.getAdapterPosition()).getVideoDuration();
        if (time.contains(":"))
            holder.tvDuration.setText(String.format(Locale.ENGLISH, "%s min", time));
        else
            holder.tvDuration.setText(String.format(Locale.ENGLISH, "%s sec", time));

        holder.imageView.setClipToOutline(true);

        if (arrayList != null) {
            Picasso.get()
                    .load(Constants.getYoutubeThumbnail(arrayList.get(position).getVideoId()))
                    .into(holder.imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            hideProgress(holder);
                        }

                        @Override
                        public void onError(Exception e) {
                            if (arrayList != null) {
                                Picasso.get()
                                        .load("https://img.youtube.com/vi/" + Constants.decode(arrayList.get(position).getVideoId()) + "/mqdefault.jpg")
                                        .into(holder.imageView, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                        hideProgress(holder);
                                                    }

                                                    @Override
                                                    public void onError(Exception e) {
                                                        if (arrayList != null) {
                                                            Picasso.get()
                                                                    .load("https://img.youtube.com/vi/" + Constants.decode(arrayList.get(position).getVideoId()) + "/sddefault.jpg")
                                                                    .into(holder.imageView, new Callback() {
                                                                                @Override
                                                                                public void onSuccess() {
                                                                                    hideProgress(holder);
                                                                                }

                                                                                @Override
                                                                                public void onError(Exception e) {
                                                                                    Picasso.get().load(R.color.colorBlack).into(holder.imageView);
                                                                                    hideProgress(holder);
                                                                                }
                                                                            }
                                                                    );
                                                        }
                                                    }
                                                }
                                        );
                            }
                        }
                    });
        }

        holder.imageView.setOnClickListener(v -> {
            Intent intent=new Intent(v.getContext(), VideoPreview.class);
            intent.putExtra("video", Constants.decode(arrayList.get(holder.getAdapterPosition()).getVideoId()));
            intent.putExtra("video_name", (arrayList.get(holder.getAdapterPosition()).getVideoTitle()));
            intent.putExtra("video_type", (arrayList.get(holder.getAdapterPosition()).getVideoCategory()));
            intent.putExtra("video_shots", (arrayList.get(holder.getAdapterPosition()).getVideoShots()));
            intent.putExtra("video_description", (arrayList.get(holder.getAdapterPosition()).getVideoDetails()));
            intent.putExtra("script_url", (arrayList.get(holder.getAdapterPosition()).getScriptUrl()));
            v.getContext().startActivity(intent);
        });


    }

    private void hideProgress(ViewHolder holder) {
        holder.loader.setVisibility(View.GONE);
        holder.ivPlay.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void clear() {
        arrayList.clear();
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView, ivPlay;
        private TextView tvVideoName, tvDuration;
        private MKLoader loader;

        ViewHolder(View view) {
            super(view);
            this.tvVideoName = view.findViewById(R.id.rowVideos_tvVideoName);
            this.tvDuration = view.findViewById(R.id.rowVideos_tvDuration);
            this.loader = view.findViewById(R.id.rowVideos_loader);
            this.imageView = view.findViewById(R.id.rowVideos_imageView);
            this.ivPlay = view.findViewById(R.id.rowVideos_ivPlay);
        }
    }

}

