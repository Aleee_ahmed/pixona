package com.systema.pixona.Views.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.systema.pixona.R;

public class IdeasMenu extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ideas_menu);

        initComponents();

    }

    private void initComponents() {
        Button btnRealEstate = findViewById(R.id.ideasMenu_btnRealEstate);
        Button btnAuto = findViewById(R.id.ideasMenu_btnAuto);
        Button btnMortgage = findViewById(R.id.ideasMenu_btnMortgage);
        Button btnFood = findViewById(R.id.ideasMenu_btnFood);
        Button btnInsurance = findViewById(R.id.ideasMenu_btnInsurance);
        Button btnLaw = findViewById(R.id.ideasMenu_btnLaw);
        Button btnMusic = findViewById(R.id.ideasMenu_btnMusic);
        Button btnEvent = findViewById(R.id.ideasMenu_btnEvent);
        Button btnOther = findViewById(R.id.ideasMenu_btnOther);
        TextView tvContactUs = findViewById(R.id.ideasMenu_tvContactUs);
        TextView tvNeedHelp = findViewById(R.id.ideasMenu_tvNeedHelp);

        btnRealEstate.setOnClickListener(this);
        btnAuto.setOnClickListener(this);
        btnMortgage.setOnClickListener(this);
        btnFood.setOnClickListener(this);
        btnInsurance.setOnClickListener(this);
        btnLaw.setOnClickListener(this);
        btnMusic.setOnClickListener(this);
        btnEvent.setOnClickListener(this);
        btnOther.setOnClickListener(this);
        tvContactUs.setOnClickListener(this);
        tvNeedHelp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ideasMenu_btnRealEstate:
                btnRealEstateClick();
                break;

            case R.id.ideasMenu_btnAuto:
                btnAutoClick();
                break;

            case R.id.ideasMenu_btnMortgage:
                btnMortgageClick();
                break;

            case R.id.ideasMenu_btnFood:
                btnFoodClick();
                break;

            case R.id.ideasMenu_btnInsurance:
                btnInsuranceClick();
                break;

            case R.id.ideasMenu_btnLaw:
                btnLawClick();
                break;

            case R.id.ideasMenu_btnMusic:
                btnMusicClick();
                break;

            case R.id.ideasMenu_btnEvent:
                btnEventClick();
                break;

            case R.id.ideasMenu_btnOther:
                btnOtherClick();
                break;

            case R.id.ideasMenu_tvContactUs:
                startActivity(new Intent(IdeasMenu.this, ContactUs.class));
                break;

            case R.id.ideasMenu_tvNeedHelp:
                startActivity(new Intent(IdeasMenu.this, ContactUs.class));
                break;

        }
    }


    private void btnRealEstateClick() {
        startActivity(new Intent(this, Videos.class)
                .putExtra("category","Real Estate"));
    }

    private void btnAutoClick() {
        startActivity(new Intent(this, Videos.class)
                .putExtra("category","Auto"));
    }

    private void btnMortgageClick() {
        startActivity(new Intent(this, Videos.class)
                .putExtra("category","Mortgage"));
    }

    private void btnFoodClick() {
        startActivity(new Intent(this, Videos.class)
                .putExtra("category","Food"));
    }

    private void btnInsuranceClick() {
        startActivity(new Intent(this, Videos.class)
                .putExtra("category","Insurance"));

    }
    private void btnLawClick() {
        startActivity(new Intent(this, Videos.class)
                .putExtra("category","Law"));
    }

    private void btnMusicClick() {
        startActivity(new Intent(this, Videos.class)
                .putExtra("category","Music"));
    }

    private void btnEventClick() {
        startActivity(new Intent(this, Videos.class)
                .putExtra("category","Event"));
    }

    private void btnOtherClick() {
        startActivity(new Intent(this, Videos.class)
                .putExtra("category","Other"));
    }
}
