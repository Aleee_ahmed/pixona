package com.systema.pixona.Views.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.systema.pixona.Interfaces.IVideoDetails;
import com.systema.pixona.Models.VideoDetailsModel;
import com.systema.pixona.Presenter.VideoDetailsPresenter;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.Internet;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AdminVideoSaving extends AppCompatActivity implements IVideoDetails.iVideoDetailsMain {

    private Spinner spCategory;
    private CheckBox checkBoxPushNotifications;
    private EditText etTitle, etLink,  etShotsNeeded, etDetails;
    private MKLoader loader;
    private Button btnSave, btnChooseFile;
    private final static int PICK_PDF_REQUEST = 101;
    public static Uri filePath = null;
    private String duration = "";
    private String category = "";
    private String time = "";
    private VideoDetailsPresenter presenter;

    private void init() {
        spCategory = findViewById(R.id.adminVideoSaving_spCategory);
        etTitle = findViewById(R.id.adminVideoSaving_etVideoTitle);
        etLink = findViewById(R.id.adminVideoSaving_etVideoLink);
        etShotsNeeded = findViewById(R.id.adminVideoSaving_etShortneeded);
        etDetails = findViewById(R.id.adminVideoSaving_etVideodetails);
        btnSave = findViewById(R.id.adminVideoSaving_btnSave);
        btnChooseFile = findViewById(R.id.adminVideoSaving_btnChooseFile);
        loader = findViewById(R.id.adminVideoSaving_loader);
        checkBoxPushNotifications = findViewById(R.id.pushNotifications_checkBoxPushNotification);

        presenter = new VideoDetailsPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_video_saving);

        init();

        btnChooseFile.setOnClickListener(v -> checkPermissions());

        btnSave.setOnClickListener(v -> {

            if (!Internet.isConnected(this)) {
                Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
                return;
            }
            if (spCategory.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.category_prompt))) {
                Toast.makeText(AdminVideoSaving.this, "Please select category.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (etTitle.getText().toString().trim().isEmpty()) {
                Toast.makeText(AdminVideoSaving.this, "Title can't be empty.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (etLink.getText().toString().trim().isEmpty()) {
                Toast.makeText(AdminVideoSaving.this, "Url link can't be empty.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (etDetails.getText().toString().trim().isEmpty()) {
                Toast.makeText(AdminVideoSaving.this, "Details can't be empty.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (etShotsNeeded.getText().toString().trim().isEmpty()) {
                Toast.makeText(AdminVideoSaving.this, "Url can't be empty.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (filePath == null) {
                Toast.makeText(AdminVideoSaving.this, "Choose file.", Toast.LENGTH_SHORT).show();
                return;
            }

            presenter.fetchVideoDetails(Constants.splitVideoIdFromURL(etLink.getText().toString().trim()));

        });


    }

    private void UploadPdf() {

        time = String.valueOf(System.currentTimeMillis());

        category = spCategory.getSelectedItem().toString().toLowerCase();
        category = category.replace(" ", "_");

        FirebaseStorage.getInstance()
                .getReference("pixona")
                .child(category)
                .child(time)
                .putFile(filePath)
                .addOnSuccessListener(taskSnapshot -> addToVideoTable())
                .addOnProgressListener(taskSnapshot -> {
                    long totalByteCount = taskSnapshot.getTotalByteCount();
                    long bytesTransferred = taskSnapshot.getBytesTransferred();

                    long percentage = (bytesTransferred * 100) / totalByteCount;

                    Toast.makeText(this, String.format(Locale.ENGLISH,"Uploading file %d%%", percentage), Toast.LENGTH_LONG).show();

                })
                .addOnFailureListener(e -> {
                    hideProgress();
                    Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                });

    }

    @NonNull
    private Task<Uri> addToVideoTable() {
        return FirebaseStorage.getInstance().getReference("pixona")
                .child(category)
                .child(time)
                .getDownloadUrl()
                .addOnSuccessListener(uri ->
                        {
                            String videoId = Constants.splitVideoIdFromURL(etLink.getText().toString().trim());

                            Map<String, String> map = new HashMap<>();
                            map.put("title", etTitle.getText().toString().trim());
                            map.put("details", etDetails.getText().toString().trim());
                            map.put("shots", etShotsNeeded.getText().toString().trim());
                            map.put("script_url", uri.toString());
                            map.put("duration", duration);

                            Map map1 = new HashMap();
                            map1.put(category + "/" + videoId, map);

                            FirebaseDatabase.getInstance().getReference("videos")
                                    .updateChildren(map1, (databaseError, databaseReference) -> {
                                        if (databaseError == null) {
                                            if (checkBoxPushNotifications.isChecked()) {
                                                addToNotificationsTable();
                                            } else {
                                                Toast.makeText(AdminVideoSaving.this, "Video uploaded successfully.", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }
                                        } else {
                                            Toast.makeText(AdminVideoSaving.this, "Something went wrong. Try again.", Toast.LENGTH_SHORT).show();
                                        }
                                        hideProgress();
                                    });
                        }
                )
                .addOnFailureListener(e -> {
                    hideProgress();
                    Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                });
    }

    private void addToNotificationsTable() {

        HashMap<String, String> map = new HashMap<>();
        map.put("title", String.format(Locale.ENGLISH, "New video has been uploaded in %s", spCategory.getSelectedItem()));

        FirebaseDatabase.getInstance().getReference("notifications")
                .child(String.valueOf(System.currentTimeMillis()))
                .setValue(map, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        Toast.makeText(this, "Video uploaded and notification is sent to all users.", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                });


    }


    private void checkPermissions() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{permission}, 1);

        } else {
            openFilePicker();
        }
    }

    private void openFilePicker() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select PDF"), PICK_PDF_REQUEST);

    }

    @Override
    public void showProgress() {
        loader.setVisibility(View.VISIBLE);
        btnSave.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        loader.setVisibility(View.GONE);
        btnSave.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayAddTravelPlan(VideoDetailsModel videoDetailsModel) {
        duration = Constants.getDuration(videoDetailsModel.getItems().get(0).getContentDetails().getDuration());
        UploadPdf();
    }

    @Override
    public void displayError(Throwable throwable) {
        Toast.makeText(this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            btnChooseFile.setVisibility(View.GONE);
            findViewById(R.id.adminVideoSaving_tvFileSelected).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == 1) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                openFilePicker();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

}
