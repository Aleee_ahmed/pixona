package com.systema.pixona.Views.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kal.rackmonthpicker.MonthType;
import com.kal.rackmonthpicker.RackMonthPicker;
import com.systema.pixona.Network.PostAPIHandlerAsync;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.Internet;
import com.systema.pixona.Utils.Json;
import com.systema.pixona.Utils.SnackBarr;
import com.systema.pixona.Utils.Util;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SignUp extends AppCompatActivity  implements PostAPIHandlerAsync.PostAsyncResponse {
    private EditText etName, etEmail, etPassword, etCreditCard, etCode, etZip,etPromoCode;
    private TextView etExDate;
    private MKLoader loader;
    private String firebaseToken;
    private String email = "";

    private void initComponents() {
        etName = findViewById(R.id.signUp_etName);
        etEmail = findViewById(R.id.signUp_etEmail);
        etPassword = findViewById(R.id.signUp_etPassword);
        etCreditCard = findViewById(R.id.signUp_etCreditCard);
        etCode = findViewById(R.id.signUp_etCode);
        etZip = findViewById(R.id.signUp_etZip);
        etExDate = findViewById(R.id.signUp_etExDate);
        loader = findViewById(R.id.signUp_loader);
        etPromoCode = findViewById(R.id.signUp_etPromoCode);
        FirebaseInstanceId
                .getInstance()
                .getInstanceId()
                .addOnSuccessListener(
                        instanceIdResult -> firebaseToken = instanceIdResult.getToken()
                );

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initComponents();

        HashMap<String, Object> eventValue = new HashMap<>();
        AppsFlyerLib.getInstance().trackEvent(this, "af_on_registration", eventValue);

    }

    private boolean validateFields(){

        if (TextUtils.isEmpty(etName.getText())){
            etName.setError("Name Should'nt be empty");
            return false;
        }

        if (TextUtils.isEmpty(etEmail.getText())){
            etEmail.setError("Email should'nt be empty");
            return false;
        }

        if (TextUtils.isEmpty(etPassword.getText())){
            etPassword.setError("Password should'nt be empty");
            return false;
        }

        if (TextUtils.isEmpty(etCreditCard.getText())){
            etCreditCard.setError("Credit Card Number should'nt be empty");
            return false;
        }

        if (TextUtils.isEmpty(etCode.getText())){
            etCode.setError("Code should'nt be empty");
            return false;
        }

        if (TextUtils.isEmpty(etZip.getText())){
            etZip.setError("Zip Code should'nt be empty");
            return false;
        }

        if (TextUtils.isEmpty(etExDate.getText())){
            etExDate.setError("Expiration Date should'nt be empty");
            return false;
        }

        if (TextUtils.isEmpty(etPromoCode.getText())){
            etExDate.setError("Promo code should'nt be empty");
            return false;
        }

        return true;
    }


    public void btnSignUpClick(View view) {

        if (Internet.isConnected(this)) {

            if (validateFields()) {

                loader.setVisibility(View.VISIBLE);

                checkDuplicateUser();

            }

        } else {
            Toast.makeText(this, "Internet not connected.", Toast.LENGTH_SHORT).show();
        }


    }

    private void uploadToSmartSheet() {

        String json = Json.createSignUpJSON(
                etName.getText().toString(),
                etEmail.getText().toString(),
                etPassword.getText().toString(),
                etCreditCard.getText().toString(),
                etCode.getText().toString(),
                etZip.getText().toString(),
                etExDate.getText().toString(),
                etPromoCode.getText().toString()
        );

        String url="https://api.smartsheet.com/2.0/sheets/300230949267332/rows/"; //Pixona app sign ups


        new PostAPIHandlerAsync(url, json, "POST", this).execute();

    }

    private void checkDuplicateUser() {
        email = Constants.encode(etEmail.getText().toString().toLowerCase().trim());

        DatabaseReference referenceAlreadyExistEmail = FirebaseDatabase.getInstance().getReference("users");

        referenceAlreadyExistEmail.child(email)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() == null) {
                            uploadToSmartSheet();
                        } else {
                            loader.setVisibility(View.GONE);
                            Toast.makeText(SignUp.this, "User with " + Constants.decode(email) + " already exist.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        loader.setVisibility(View.GONE);
                        Toast.makeText(SignUp.this, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void uploadToFirebase() {
        Map<String, String> usersTableData = new HashMap<>();
        usersTableData.put("ccv", Constants.encode(etCode.getText().toString()));
        usersTableData.put("credit_card_number", Constants.encode(etCreditCard.getText().toString()));
        usersTableData.put("expiration_date",  etExDate.getText().toString());
        usersTableData.put("name",  etName.getText().toString());
        usersTableData.put("password",  etPassword.getText().toString());
        usersTableData.put("zip_code",  etZip.getText().toString());
        usersTableData.put("status",  Constants.PENDING);
        usersTableData.put("device_id",  firebaseToken);
        usersTableData.put("joining_date",  Util.currentDate());


        FirebaseDatabase.getInstance().getReference("users")
                .child(email)
                .setValue(usersTableData, (databaseError, databaseReference) -> {

                    if (databaseError == null) {
                        Toast.makeText(SignUp.this, "Sign up successfully.", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(SignUp.this,Thanks.class);
                        intent.putExtra("From","SignUp");
                        //AppsFlyer Event
                        HashMap<String, Object> eventValue = new HashMap<String, Object>();
                        eventValue.put(AFInAppEventType.COMPLETE_REGISTRATION, AFInAppEventType.COMPLETE_REGISTRATION);
                        AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.COMPLETE_REGISTRATION,eventValue);

                        startActivity(intent);
                        finish();
                    } else {
                        loader.setVisibility(View.GONE);
                        Toast.makeText(SignUp.this,
                                "Error = " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                });
    }

    public void pickDate(View view1) {

        //                .setPositiveText("Select")
//                .setColorTheme(R.color.color_primary)
        //.setNegativeText("Select")
        new RackMonthPicker(this)
                .setLocale(Locale.ENGLISH)
                .setMonthType(MonthType.NUMBER)
                .setPositiveButton((month, startDate, endDate, year, monthLabel) -> {
                    if (month < 9) {
                        etExDate.setText(new StringBuilder()
                                .append("0").append(month).append("/")
                                .append(year));
                    } else {
                        etExDate.setText(new StringBuilder()
                                .append( month).append("/")
                                .append(year));
                    }
                })
                .setNegativeButton(Dialog::dismiss)
                .show();
    }

    @Override
    public void postProcessFinished(String output) {

        if (output.contains("SUCCESS")) {
            uploadToFirebase();
        } else if (output.contains("Exception")) {
            SnackBarr.showErrorSnackBar(etName, this, output);
        } else {
            SnackBarr.showErrorSnackBar(etName, this, "Something went wrong. Please submit data again.");
        }

    }


}
