package com.systema.pixona.Views.activities;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.systema.pixona.R;
import com.systema.pixona.Utils.MirroredTextView;

public class TestScrolling extends AppCompatActivity {

    private float size = 24f;
    private View view;
    private Handler customHandler;
    private boolean delayDone;
    private Runnable scroll = new scrollRunnable();
    private int scrollSpeed;
    private boolean scrollText;
    private int time;
    private int timer;
    private String TAG;
    private TextView textViewToChange, tvSize;
    private ScrollView scrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_scrolling);


        scrollView = findViewById(R.id.ScrollerTeleprompter);

        textViewToChange = findViewById(R.id.TextTeleprompter);

        scrollSpeed = 21;
        MirroredTextView.mirror = false;
        textViewToChange.setLineSpacing((float) (10), 1.0f);
        textViewToChange.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto.ttf"), Typeface.NORMAL);
        textViewToChange.setTextSize(size);
        textViewToChange.setTextColor(Color.parseColor("#FFFFFF"));
        textViewToChange.setBackgroundColor(Color.parseColor("#700A0A0A"));

        time = scrollSpeed * 100;
        scrollText = false;
        delayDone = true;

        scrollView.setBackgroundColor(Color.parseColor("#700A0A0A"));
        scrollView.getChildAt(0).setOnClickListener(new clicked());

        customHandler = new Handler();

    }


    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 1) {
            tap();
        }
        return true;
    }

    private void tap() {
        Log.d(TAG, "tap() called");
        if (delayDone) {
            scrollText = !scrollText;
            customHandler.post(scroll);

        }
    }

    class scrollRunnable implements Runnable {
        scrollRunnable() {}

        public void run() {
//            Log.d(TAG, "run() called");
            if (scrollText) {
                if (timer > 0) {
                    timer = timer - 1;
                } else {
                    scrollView.scrollTo(0, scrollView.getScrollY() + 1);
                    timer = time;
                }
                customHandler.post(scroll);
            }
        }
    }

    class clicked implements View.OnClickListener {
        clicked() {
        }
        public void onClick(View view) {
            Log.d(TAG, "onClick() called with: view = [" + view + "]");
            tap();
        }
    }


}
