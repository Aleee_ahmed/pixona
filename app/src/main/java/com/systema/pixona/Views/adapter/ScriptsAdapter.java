package com.systema.pixona.Views.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.systema.pixona.Models.ScriptsModel;
import com.systema.pixona.R;

import java.util.ArrayList;

public class ScriptsAdapter extends RecyclerView.Adapter<ScriptsAdapter.ViewHolder> {

    private ArrayList<ScriptsModel> arrayList;

    public ScriptsAdapter(ArrayList<ScriptsModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_scripts, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvScriptNumber.setText(arrayList.get(holder.getAdapterPosition()).getScriptNumber());
        holder.tvScriptName.setText(arrayList.get(holder.getAdapterPosition()).getScriptName());
        holder.tvScriptEdittedOn.setText(arrayList.get(holder.getAdapterPosition()).getScriptEdittedOn());

        holder.llEdit.setOnClickListener(v -> Toast.makeText(v.getContext(), "Edit clicked..", Toast.LENGTH_SHORT).show());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvScriptNumber, tvScriptName, tvScriptEdittedOn;
        private LinearLayout llEdit;

        ViewHolder(View view) {
            super(view);

            tvScriptNumber = view.findViewById(R.id.rowScript_tvScriptNumber);
            tvScriptName = view.findViewById(R.id.rowScript_tvScriptName);
            tvScriptEdittedOn = view.findViewById(R.id.rowScript_tvScriptEditedOn);

            llEdit = view.findViewById(R.id.rowScript_llEdit);

        }
    }

}

