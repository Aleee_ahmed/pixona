package com.systema.pixona.Views.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.SharedPreference;
import com.tuyenmonkey.mkloader.MKLoader;

public class ChangePassword extends AppCompatActivity {

    private EditText etOldPassword,etNewPassword,etConfirmPassword;
    private Button btnSave;
    private MKLoader loader;

    private void initComponents() {
        etOldPassword = findViewById(R.id.changePassword_etOldPassword);
        etNewPassword = findViewById(R.id.changePassword_etNewPassword);
        etConfirmPassword = findViewById(R.id.changePassword_etConfirmPassword);
        btnSave = findViewById(R.id.changePassword_btnSave);
        loader = findViewById(R.id.changePassword_loader);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        initComponents();

        btnSave.setOnClickListener(v -> {
            String oldPass = etOldPassword.getText().toString();
            String newPass = etNewPassword.getText().toString();
            String confirmPass = etConfirmPassword.getText().toString();

            if (oldPass.isEmpty()) {
                Toast.makeText(ChangePassword.this, "Enter current password", Toast.LENGTH_SHORT).show();
                return;
            }
            if (newPass.isEmpty()) {
                Toast.makeText(ChangePassword.this, "Enter new password", Toast.LENGTH_SHORT).show();
                return;
            }
            if (confirmPass.isEmpty()) {
                Toast.makeText(ChangePassword.this, "Enter confirm password", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!oldPass.equals(SharedPreference.getLoginUserPassword(this))) {
                Toast.makeText(this, "You entered a wrong current password.", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!newPass.equals(confirmPass)) {
                Toast.makeText(ChangePassword.this, "New password and confirm password aren't same", Toast.LENGTH_SHORT).show();
                return;
            }

            showProgress();

            updatePassword(newPass);


        });

    }

    private void updatePassword(String newPass) {
        FirebaseDatabase.getInstance().getReference("users")
                .child(Constants.encode(SharedPreference.getLoginUserEmail(this)))
                .child("password")
                .setValue(newPass, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        Toast.makeText(this, "Password updated successfully.", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                });
    }

    private void showProgress() {
        btnSave.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        btnSave.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }

}
