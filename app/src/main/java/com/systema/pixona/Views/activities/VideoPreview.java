package com.systema.pixona.Views.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.systema.pixona.R;
import com.systema.pixona.Utils.QueryHandler;
import com.systema.pixona.Utils.YouTubeFailureRecoveryActivity;
import com.systema.pixona.Data.TeleprompterFileContract;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.systema.pixona.Utils.Constants.Youtube_API_KEY;

public class VideoPreview extends YouTubeFailureRecoveryActivity
        implements YouTubePlayer.OnInitializedListener,
        QueryHandler.onQueryHandlerInsertComplete {

    private String videoUrl, videoDescription, videoType, videoName, scriptUrl, videoShots;
    private TextView tvType, tvInstructions, tvVideoTitle;
    private Button btnShotsNeeded, btnRecordThisVideo;
    private MKLoader loader;
    private StringBuilder builder = new StringBuilder();


    private void init() {
        tvType = findViewById(R.id.videoPreview_tvType);
        tvInstructions = findViewById(R.id.videoPreview_tvInstructions);
        tvVideoTitle = findViewById(R.id.videoPreview_tvVideoTitle);
        btnRecordThisVideo = findViewById(R.id.videoPreview_btnRecordThisVideo);
        btnShotsNeeded = findViewById(R.id.videoPreview_btnShotsNeeded);
        loader = findViewById(R.id.videoPreview_loader);

        YouTubePlayerView youTubePlayerView = findViewById(R.id.videoPreview_youtubePlayer);
        youTubePlayerView.initialize(Youtube_API_KEY, this);
        youTubePlayerView.animate();

        videoUrl = getIntent().getStringExtra("video");
        videoName = getIntent().getStringExtra("video_name");
        videoType = getIntent().getStringExtra("video_type");
        videoShots = getIntent().getStringExtra("video_shots");
        videoDescription = getIntent().getStringExtra("video_description");
        scriptUrl = getIntent().getStringExtra("script_url");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_preview);

        init();

        tvType.setText(videoType);
        tvInstructions.setText(videoDescription);
        tvVideoTitle.setText(videoName);

        getText(scriptUrl);

        btnShotsNeeded.setOnClickListener(v -> showShotsNeeded());

        btnRecordThisVideo.setOnClickListener(v -> {
            if (!builder.toString().isEmpty()) {
                showProgress();
//                saveDataToDb(videoName + "_" + new Random().nextInt((1000 - 999) + 999), builder.toString());
                saveDataToDb(videoName,builder.toString());

            }
        });

    }

    private void showShotsNeeded() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert);
        builder.setTitle("Xunami");
        builder.setMessage(videoShots);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer youTubePlayer, boolean wasDestroyed) {

        youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        youTubePlayer.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);

        if (!wasDestroyed){
            youTubePlayer.loadVideo(videoUrl);
        }

        youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(String s) {
                youTubePlayer.play();
            }

            @Override
            public void onAdStarted() {

            }

            @Override
            public void onVideoStarted() {

            }

            @Override
            public void onVideoEnded() {
                youTubePlayer.loadVideo(videoUrl);
            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {
                youTubePlayer.play();
            }
        });

        youTubePlayer.setOnFullscreenListener(b1 -> {

            youTubePlayer.play();

            if (b1) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                hideSystemUI();
                youTubePlayer.setFullscreen(true);
                youTubePlayer.play();

            }else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                showSystemUI();
                youTubePlayer.play();
            }

        });



    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult youTubeInitializationResult) {
        try {
            Intent intent = YouTubeStandalonePlayer.createVideoIntent(this, Youtube_API_KEY,videoUrl);
            startActivity(intent);
            finish(); //to exit current Activity in which YouTubeFragment is not working
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return  (YouTubePlayerView) findViewById(R.id.videoPreview_youtubePlayer);
    }


    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }


    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private void getText(String videoUrl) {
        new Thread(() -> {
            showProgress();

            try {

                URL url = new URL(videoUrl);
                HttpURLConnection conn=(HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(60000);

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                String str;
                while ((str = in.readLine()) != null) {
                    builder.append(str).append("\n");
                    Log.e("MyTag", builder.toString());
                }

                runOnUiThread(this::hideProgress);

                in.close();
            } catch (Exception e) {
                Log.d("MyTag",e.toString());
            }


        }).start();

    }

    private void showProgress() {
        loader.setVisibility(View.VISIBLE);
        btnRecordThisVideo.setVisibility(View.INVISIBLE);
    }

    private void hideProgress() {
        btnRecordThisVideo.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }

    private void saveDataToDb(String title, String description) {

//        QueryHandler queryHandler = new QueryHandler(getContentResolver(), VideoPreview.this);
//
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_TITLE, title);
//        contentValues.put(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_CONTENT, description);
//        contentValues.put(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_IMAGE, android.R.drawable.ic_menu_camera);
//
//
//        queryHandler.startInsert(1, null, TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI, contentValues);

        Intent intent=new Intent(VideoPreview.this, NewTeleprompter.class);
        intent.putExtra("From","VideoPreview");
        intent.putExtra("Title",title);
        intent.putExtra("Description",description);
        intent.putExtra("URI",TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI);
        startActivity(intent);
//
    }

    @Override
    public void onInsertComplete(int token, Object cookie, Uri uri) {
        if (uri == null) {
            Toast.makeText(VideoPreview.this, R.string.failed_to_save_data, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(VideoPreview.this, R.string.saved_successfully, Toast.LENGTH_SHORT).show();
            finish();
        }
        hideProgress();
    }

    @Override
    public void onUpdateComplete(int token, Object cookie, int result) {

    }
}
