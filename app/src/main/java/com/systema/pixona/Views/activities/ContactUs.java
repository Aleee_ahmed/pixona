package com.systema.pixona.Views.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.systema.pixona.Interfaces.ISendEmail;
import com.systema.pixona.Presenter.SendEmailPresenter;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.Internet;
import com.tuyenmonkey.mkloader.MKLoader;

public class ContactUs extends AppCompatActivity implements ISendEmail.iSendEmailMain {

    private EditText etName, etMail, etPhone, etMessage;
    private Button btnSend;
    private MKLoader loader;
    private SendEmailPresenter presenter;

    private void initComponents() {
        presenter = new SendEmailPresenter(this);
        etName = findViewById(R.id.contactUs_etName);
        etMail = findViewById(R.id.contactUs_etMail);
        etPhone = findViewById(R.id.contactUs_etPhone);
        etMessage = findViewById(R.id.contactUs_etWriteSomething);
        btnSend = findViewById(R.id.contactUs_btnSend);
        loader = findViewById(R.id.contactUs_loader);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        initComponents();

        btnSend.setOnClickListener(v -> {
            if (validateFields()) {

                if (!Internet.isConnected(this)) {
                    Toast.makeText(this, "No internet connectivity. Please check your internet connection.", Toast.LENGTH_SHORT).show();
                    return;
                }

                showProgress();

                sendEmail(createMessage());

            } else {
                Toast.makeText(this, "Fill all fields to continue.", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private boolean validateFields() {
        return !etMessage.getText().toString().isEmpty() &&
                !etPhone.getText().toString().isEmpty() &&
                !etMail.getText().toString().isEmpty() &&
                !etName.getText().toString().isEmpty();
    }


    private String createMessage() {
        StringBuilder builder;
        builder = new StringBuilder();
        builder.append("Name: ").append(etName.getText().toString()).append("\n\n");
        builder.append("Email: ").append(etMail.getText().toString()).append("\n\n");
        builder.append("Phone: ").append(etPhone.getText().toString()).append("\n\n");
        builder.append("Message: ").append("\n").append(etMessage.getText().toString()).append("\n");
        return builder.toString();
    }


    private void sendEmail(String message) {
        presenter.sendEmail(Constants.EMAIL, "Xunami: Contact us", message);
    }

    @Override
    public void showProgress() {
        etName.setEnabled(false);
        etMail.setEnabled(false);
        etPhone.setEnabled(false);
        etMessage.setEnabled(false);
        btnSend.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        etName.setEnabled(true);
        etMail.setEnabled(true);
        etPhone.setEnabled(true);
        etMessage.setEnabled(true);
        btnSend.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }

    @Override
    public void displayResult(String model) {
        if (model.contains("Thank you")) {
            startActivity(new Intent(this, Thanks.class).putExtra("From", "ContactUs"));
            finish();
        } else {
            Toast.makeText(this, "Oops.. Something went wrong. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
