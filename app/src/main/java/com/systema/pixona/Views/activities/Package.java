package com.systema.pixona.Views.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AppsFlyerLib;
import com.systema.pixona.R;

import java.util.HashMap;

public class Package extends AppCompatActivity {

    private Button btnSignUp;
    private CheckBox checkBox;
    private TextView tvTerms,tvPolicy;



    private void init() {
        btnSignUp = findViewById(R.id.package_btnSignUp);
        checkBox = findViewById(R.id.package_cbPolicy);
        tvTerms=findViewById(R.id.package_tvTerms);
        tvPolicy=findViewById(R.id.package_tvPolicy);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package);

        init();

//        Map<String, Object> eventValue = new HashMap<>();
//        eventValue.put(AFInAppEventParameterName.REVENUE,1234.56);
//        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, "Shirt");
//        eventValue.put(AFInAppEventParameterName.CONTENT_ID,"1234567");
//        eventValue.put(AFInAppEventParameterName.CURRENCY,"USD");
//        AppsFlyerLib.getInstance().trackEvent(getApplicationContext() , AFInAppEventType.PURCHASE , eventValue);

        HashMap<String, Object> eventValue = new HashMap<>();
        eventValue.put(AFInAppEventParameterName.PAYMENT_INFO_AVAILIBLE, "49.99");
        AppsFlyerLib.getInstance().trackEvent(this, "af_started_registration", eventValue);


        btnSignUp.setOnClickListener(v -> {
            if (!checkBox.isChecked()) {
                Toast.makeText(this, "Please agree to Terms of Services and Privacy Policy.", Toast.LENGTH_SHORT).show();
                return;
            }

            startActivity(new Intent(this, SignUp.class));
            finish();

        });

        View.OnClickListener onClickListener = v -> startActivity(new Intent(Package.this, WebView.class));
        tvTerms.setOnClickListener(onClickListener);
        tvPolicy.setOnClickListener( onClickListener);

    }
}
