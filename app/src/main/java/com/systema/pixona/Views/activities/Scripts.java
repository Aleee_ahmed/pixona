package com.systema.pixona.Views.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.systema.pixona.Models.ScriptsModel;
import com.systema.pixona.R;
import com.systema.pixona.Views.adapter.ScriptsAdapter;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

public class Scripts extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivBack;
    private LinearLayout llEdit, llMyScripts, llRecord, llSave,llEditScriptslayout,llMyScriptsLayout;

    //MyScript Variables
    private RecyclerView recyclerView;
    private MKLoader loaderMyScripts;
    private LinearLayout llWriteScript;
    private ArrayList<ScriptsModel> arrayList;

    //EditScript variables
    private EditText etTitle, etWriteSomething;
    private Button btnSave;
    private MKLoader loaderEditScripts;


    private void init() {
        ivBack = findViewById(R.id.scripts_ivBack);

        llEdit = findViewById(R.id.scripts_llEdit);
        llMyScripts = findViewById(R.id.scripts_llMyScripts);
        llRecord = findViewById(R.id.scripts_llRecord);
        llSave = findViewById(R.id.scripts_llSave);

        arrayList = new ArrayList<>();
        loaderMyScripts = findViewById(R.id.myScripts_loader);
        llWriteScript = findViewById(R.id.myScript_llWriteScript);
        recyclerView = findViewById(R.id.myScripts_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new ScriptsAdapter(arrayList));

        etTitle = findViewById(R.id.editScripts_etTitle);
        etWriteSomething = findViewById(R.id.editScripts_etWriteSomething);
        loaderEditScripts = findViewById(R.id.editScripts_loader);
        btnSave = findViewById(R.id.editScripts_btnSave);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scripts);

        init();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.scripts_ivBack:
                finish();
                break;
            case R.id.scripts_llEdit:
                llEditScriptslayout.setVisibility(View.VISIBLE);
                llMyScripts.setVisibility(View.GONE);
                break;
            case R.id.scripts_llMyScripts:
                llEditScriptslayout.setVisibility(View.GONE);
                llMyScripts.setVisibility(View.VISIBLE);
                break;
            case R.id.scripts_llRecord:
                break;
            case R.id.scripts_llSave:
        }
    }
}
