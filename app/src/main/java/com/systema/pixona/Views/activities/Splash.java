package com.systema.pixona.Views.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.SharedPreference;

public class Splash extends AppCompatActivity {

    private boolean shouldGoForwardNow = false;
    private TextView tvSkip;
    private Handler handler;

    private Runnable runnable = () -> {
        if (shouldGoForwardNow) {
            goToNextActivity();
        } else {
            shouldGoForwardNow = true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        tvSkip = findViewById(R.id.splash_tvSkip);
        VideoView view = findViewById(R.id.splash_vv);
        handler = new Handler();

        String path;

        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        int height = Resources.getSystem().getDisplayMetrics().heightPixels;

        if (width > 1140 || height > 2900) {
            path = "android.resource://" + getPackageName() + "/" + R.raw.xunami_x_large;
        } else if (width > 1080 || height > 2160) {
            path = "android.resource://" + getPackageName() + "/" + R.raw.xunami_large;
        } else if (width > 720 || height > 1280) {
            path = "android.resource://" + getPackageName() + "/" + R.raw.xunami_medium;
        } else {
            path = "android.resource://" + getPackageName() + "/" + R.raw.xunami_small;
        }

        tvSkip.setOnClickListener(view1 -> goToNextActivity());

        view.setVideoURI(Uri.parse(path));
        view.start();

        if (SharedPreference.getLoginStatus(this)) {
            if (!SharedPreference.getLoginUserEmail(this).equalsIgnoreCase(Constants.ADMIN)) {
                fetchData();
            } else {
                shouldGoForwardNow = true;
                tvSkip.setVisibility(View.VISIBLE);
            }
        } else {
            shouldGoForwardNow = true;
            tvSkip.setVisibility(View.VISIBLE);
        }


        handler.postDelayed(runnable, 11500);

    }

    private void fetchData() {
        FirebaseDatabase.getInstance()
                .getReference("users")
                .child(Constants.encode(SharedPreference.getLoginUserEmail(this)))
                .child("status")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (shouldGoForwardNow) {
                            SharedPreference.setUserStatus(Splash.this, dataSnapshot.getValue(String.class));
                            Log.e("HomePageSplash", dataSnapshot.getValue(String.class));
                            goToNextActivity();
                        } else {
                            Log.e("HomePageSplash", dataSnapshot.getValue(String.class));
                            SharedPreference.setUserStatus(Splash.this, dataSnapshot.getValue(String.class));
                            shouldGoForwardNow = true;
                            tvSkip.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        fetchData();
                    }
                });
    }

    private void goToNextActivity() {
        handler.removeCallbacks(runnable);
        Intent intent = new Intent(Splash.this, HomePage.class);
        startActivity(intent);
        finish();
    }
}
