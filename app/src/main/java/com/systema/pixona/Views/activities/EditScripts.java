package com.systema.pixona.Views.activities;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.systema.pixona.R;
import com.systema.pixona.Utils.QueryHandler;
import com.systema.pixona.Utils.TeleprompterFile;
import com.systema.pixona.Data.TeleprompterFileContract;
import com.tuyenmonkey.mkloader.MKLoader;

public class EditScripts extends AppCompatActivity implements View.OnClickListener ,
        QueryHandler.onQueryHandlerInsertComplete, LoaderManager.LoaderCallbacks<Cursor>{

    private EditText etTitle,etDescription;
    private Button btnSave;
    private MKLoader mkLoader;
    private Uri mCurrentUri;
    TeleprompterFile teleprompterFile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_script);
        initComponents();

        Intent intent = getIntent();
        if (intent!=null)
        {
            teleprompterFile=  intent.getParcelableExtra("Data");
            if (teleprompterFile!=null) {
                etTitle.setText(teleprompterFile.getTitle());
                etDescription.setText(teleprompterFile.getContent());
            }
            mCurrentUri = intent.getData();
        }

//        if (intent != null) {
//            if (intent.hasExtra(INTENT_EXTRA_CONTENT)) {
//                String content = intent.getStringExtra(INTENT_EXTRA_CONTENT);
//                etDescription.setText(content);
//            }
//            mCurrentUri = intent.getData();
//        }

        if (mCurrentUri == null) {
//            setTitle(getString(R.string.add_a_file));
        } else {
//            setTitle(R.string.edit_a_file);
            getLoaderManager().initLoader(0, null, this);
        }
    }

    private void initComponents() {
        etTitle=findViewById(R.id.editScripts_etTitle);
        etDescription=findViewById(R.id.editScripts_etWriteSomething);
        btnSave=findViewById(R.id.editScripts_btnSave);
        mkLoader=findViewById(R.id.editScripts_loader);

        btnSave.setOnClickListener(this);
    }


    private void saveDataToDb() {
        final String title = etTitle.getText().toString();
        String content = etDescription.getText().toString();

        QueryHandler queryHandler = new QueryHandler(getContentResolver(), EditScripts.this);

        ContentValues contentValues = new ContentValues();
        contentValues.put(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_TITLE, title);
        contentValues.put(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_CONTENT, content);
        contentValues.put(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_IMAGE, android.R.drawable.ic_menu_camera);

        if (mCurrentUri == null) {
            queryHandler.startInsert(1, null, TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI, contentValues);
        } else {
            queryHandler.startUpdate(1, null, mCurrentUri, contentValues, null, null);
        }
//        if (teleprompterFile==null){
//            queryHandler.startInsert(1, null, TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI, contentValues);
//        }else {
//            queryHandler.startUpdate(1, null, mCurrentUri, contentValues, null, null);
//        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.editScripts_btnSave:
                saveDataToDb();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String projection[] = {
                TeleprompterFileContract.TeleprompterFileEvent._ID,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_TITLE,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_CONTENT,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_IMAGE,
                TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_IS_FAV,
        };
        return new CursorLoader(this,
                TeleprompterFileContract.TeleprompterFileEvent.CONTENT_URI,
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            // The Cursor is now set to the right position
            int idColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent._ID);
            int titleColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_TITLE);
            int contentColumnIndex = cursor.getColumnIndex(TeleprompterFileContract.TeleprompterFileEvent.COLUMN_FILE_CONTENT);

            // Extract out the value from the Cursor for the given column index
            String id = cursor.getString(idColumnIndex);
            String title = cursor.getString(titleColumnIndex);
            String content = cursor.getString(contentColumnIndex);

//            etTitle.setText(title);
//            etDescription.setText(content);
//            setImage();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        etTitle.setText("");
        etDescription.setText("");

    }

    @Override
    public void onInsertComplete(int token, Object cookie, Uri uri) {
        final String title = etTitle.getText().toString();
        if (uri == null) {
//            Analytics.logEventAddFileToDb(this, FAIL);
            Toast.makeText(EditScripts.this, R.string.failed_to_save_data, Toast.LENGTH_SHORT).show();
        } else {
//            if (bitmap != null) {
//                saveImageBitmap(bitmap, title);
//            }
//            Analytics.logEventAddFileToDb(this, SUCCESS);
            Toast.makeText(EditScripts.this, R.string.saved_successfully, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onUpdateComplete(int token, Object cookie, int result) {
        final String title = etTitle.getText().toString();
        if (result == 0) {
            // If no rows were affected, then there was an error with the update.
            Toast.makeText(this, R.string.update_failed, Toast.LENGTH_SHORT).show();
        } else {
            // Otherwise, the update was successful and we can display a toast.
//            if (bitmap != null) {
//                saveImageBitmap(bitmap, title);
//            }
            Toast.makeText(this, R.string.update_successful, Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
