package com.systema.pixona.Views.activities;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.systema.pixona.R;
import com.systema.pixona.Utils.TeleprompterFile;
import java.util.List;

/**
 * Created by priyankanandiraju on 8/8/17.
 */

class TeleprompterFilesAdapter extends RecyclerView.Adapter<TeleprompterFilesAdapter.TeleprompterFilesHolder> {

    private List<TeleprompterFile> mTeleprompterFileList;
    private OnFileClickListener mOnFileClickListener;

    public interface OnFileClickListener {
        void onFileClick(TeleprompterFile teleprompterFile);
        void onDownloadClick(TeleprompterFile teleprompterFile);
        void onDeleteClick(int position, TeleprompterFile teleprompterFile);
        void onEditClick(TeleprompterFile teleprompterFile);
    }

    public TeleprompterFilesAdapter(List<TeleprompterFile> teleprompterFiles, OnFileClickListener onFileClickListener) {
        mTeleprompterFileList = teleprompterFiles;
        mOnFileClickListener = onFileClickListener;
    }

    @NonNull
    @Override
    public TeleprompterFilesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_scripts, parent, false);
        return new TeleprompterFilesHolder(view);
    }

    @Override
    public void onBindViewHolder(TeleprompterFilesHolder holder, int position) {
        TeleprompterFile currentFileData = mTeleprompterFileList.get(position);
        holder.tvNum.setText(String.valueOf(position+1+"."));
        holder.tvName.setText(currentFileData.getTitle());
        holder.tvDate.setText(currentFileData.getContent());

//        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(holder.ivFileIcon.getContext());
//        String previouslyEncodedImage = shre.getString(IMAGE_DATA + currentFileData.getTitle(), "");
//        if (!previouslyEncodedImage.equalsIgnoreCase("")) {
//            byte[] b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
//            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
//            holder.ivFileIcon.setImageBitmap(bitmap);
//        } else {
//            holder.ivFileIcon.setImageBitmap(null);
//        }
    }

    @Override
    public int getItemCount() {
       return mTeleprompterFileList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void setFileData(List<TeleprompterFile> fileList) {
        mTeleprompterFileList.clear();
        mTeleprompterFileList.addAll(fileList);
        notifyDataSetChanged();
    }

    public class TeleprompterFilesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvNum;
        TextView tvName;
        TextView tvDate;
        ImageView ivEdit,ivDelete,ivDownload;

        public TeleprompterFilesHolder(View itemView) {
            super(itemView);
            tvNum=itemView.findViewById(R.id.rowScript_tvScriptNumber);
            tvName=itemView.findViewById(R.id.rowScript_tvScriptName);
            tvDate=itemView.findViewById(R.id.rowScript_tvScriptEditedOn);
            ivEdit=itemView.findViewById(R.id.row_scripts_ivEdit);
            ivDelete=itemView.findViewById(R.id.row_scripts_ivDelete);
            ivDownload=itemView.findViewById(R.id.row_scripts_ivDownload);

            itemView.setOnClickListener(this);
            ivEdit.setOnClickListener(this);
            ivDelete.setOnClickListener(this);
            ivDownload.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            TeleprompterFile teleprompterFile;
            int adapterPosition ;

            switch (view.getId()) {
                case R.id.row_scripts_ivEdit:
                    adapterPosition=getAdapterPosition();
                    teleprompterFile = mTeleprompterFileList.get(adapterPosition);
                    mOnFileClickListener.onEditClick(teleprompterFile);
                    break;
                case R.id.row_scripts_ivDownload:
                    adapterPosition=getAdapterPosition();
                    teleprompterFile = mTeleprompterFileList.get(adapterPosition);
                    mOnFileClickListener.onDownloadClick(teleprompterFile);
                    break;
                case R.id.row_scripts_ivDelete:
                    if (mTeleprompterFileList.size()>1){
                        adapterPosition=getAdapterPosition();
                        teleprompterFile = mTeleprompterFileList.get(adapterPosition);
                        mOnFileClickListener.onDeleteClick(getAdapterPosition(), teleprompterFile);

                    }else {
                        adapterPosition=getAdapterPosition();
                        teleprompterFile = mTeleprompterFileList.get(adapterPosition);
                        mOnFileClickListener.onDeleteClick(getAdapterPosition(), teleprompterFile);
                        mTeleprompterFileList.clear();
                    }
                    break;
                default:
                    adapterPosition=getAdapterPosition();
                    teleprompterFile = mTeleprompterFileList.get(adapterPosition);
                    mOnFileClickListener.onFileClick(teleprompterFile);
                    break;
            }
        }
    }
}
