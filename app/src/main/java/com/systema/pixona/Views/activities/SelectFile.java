package com.systema.pixona.Views.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.Internet;
import com.systema.pixona.Utils.SharedPreference;
import com.systema.pixona.Utils.SnackBarr;
import com.systema.pixona.Utils.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class  SelectFile extends AppCompatActivity {

    private TransferUtility transferUtility;
    private List<TransferObserver> observers;
    private RelativeLayout rlSelectFile, rlUploadFile;
    private LinearLayout llProgress;
    private TextView tvSelectedFile, tvProgress, tvContactUs, tvNeedhelp;
    private final static Integer UPLOAD_REQUEST_CODE = 101;
    private final static String TAG = "SelectFile";
    private ProgressBar progressBar;
    private int UploadCount = 0;
    private boolean FailCheck = false;
    private List<String> selectedVideos;
    private PowerManager.WakeLock wakeLock;


    //private BroadcastReceiver broadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_file);

        AWSMobileClient.getInstance().initialize(this).execute();

        initUI();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "pixona:DoNotDimScreen");

        //        broadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                String s = intent.getStringExtra(UploadService.SERVICE_MESSAGE);
//
//                if (s.equalsIgnoreCase("onError")) {
//                    onError(intent.getStringExtra("msg"));
//                } else if (s.equalsIgnoreCase("onProgressChanged")) {
//                    onProgressChanged(intent.getLongExtra("current", 0), intent.getLongExtra("total", 0));
//                } else if (s.equalsIgnoreCase("onStateChanged")) {
//                    onStateChanged((TransferState)intent.getSerializableExtra("state"));
//                }
//                // do something here.
//            }
//        };

    }

    @Override
    protected void onResume() {
        super.onResume();
        wakeLock.acquire();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //LocalBroadcastManager.getInstance(this).registerReceiver((broadcastReceiver), new IntentFilter(UploadService.SERVICE_RESULT));

        // Get the data from any transfer's that have already happened,
        initData(); //TODO uncomment this if not working in service.
    }

    @Override
    protected void onPause() {
        super.onPause();
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        wakeLock.release();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Clear transfer listeners to prevent memory leak, or
        // else this activity won't be garbage collected.
        if (observers != null && !observers.isEmpty()) {
            for (TransferObserver observer : observers) {
                observer.cleanTransferListener();
            }
        }
    }

    /**
     * Gets all relevant transfers from the Transfer Service for populating the
     * UI
     */
    public void initData() {
        // Use TransferUtility to get all upload transfers.
        observers = transferUtility.getTransfersWithType(TransferType.UPLOAD);
        TransferListener listener = new UploadListener();
        for (TransferObserver observer : observers) {
            observer.refresh();

            // For each transfer we will will createVideoDetailsJSON an entry in
            // transferRecordMaps which will display
            // as a single row in the UI
//            HashMap<String, Object> map = new HashMap<String, Object>();
//            util.fillMap(map, observer, false);

            // Sets listeners to in progress transfers

            if (TransferState.WAITING.equals(observer.getState())
                    || TransferState.WAITING_FOR_NETWORK.equals(observer.getState())
                    || TransferState.IN_PROGRESS.equals(observer.getState())) {
                observer.setTransferListener(listener);
            }
        }
    }

    private Spanned getSpannedText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(text);
        }
    }

    private void initUI() {

        TextView tvTitle = findViewById(R.id.selectFile_tvTitle);
        String title = getResources().getString(R.string.titleEnglish);
        tvTitle.setText(getSpannedText(title));


        rlSelectFile = findViewById(R.id.rlSelectFile);
        rlUploadFile = findViewById(R.id.rlUploadFile);

        tvSelectedFile = findViewById(R.id.tvSelectedFile);
        tvContactUs = findViewById(R.id.tvContactUs);
        tvNeedhelp = findViewById(R.id.tvNeedHelp);
        TextView tvUploadingFile = findViewById(R.id.tvUploading);

        llProgress = findViewById(R.id.LLProgress);

        tvProgress = findViewById(R.id.tvProgress);

        progressBar = findViewById(R.id.progress_determinate);

        Util util = new Util();
        transferUtility = util.getTransferUtility(this);


        rlSelectFile.setOnClickListener(v -> requestStorageAccess());


        rlUploadFile.setOnClickListener(v -> {
            if (tvSelectedFile.getText().toString().isEmpty()) {
                Toast.makeText(SelectFile.this, "Please select video you want to upload.", Toast.LENGTH_SHORT).show();
                return;
            }

//                beginUpload(path);
            for (int i = 0; i < selectedVideos.size(); i++) {
                beginUpload(selectedVideos.get(i));
            }
        });


        View.OnClickListener listener = v -> startActivity(new Intent(SelectFile.this, ContactUs.class));

        tvContactUs.setOnClickListener(listener);
        tvNeedhelp.setOnClickListener(listener);


    }

//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == UPLOAD_REQUEST_CODE) {
//            if (resultCode == Activity.RESULT_OK) {
//                Uri uri = data.getData();
//
//                try {
////                    path = getPath(uri);
//                    uriList=getPath(uri)
//
//                    String filename = path.substring(path.lastIndexOf("/") + 1);
//
//                    tvSelectedFile.setText(filename);
//
//                } catch (Exception e) {
//                    Toast.makeText(this,
//                            "Unable to get the file from the given URI. See error log for details",
//                            Toast.LENGTH_LONG).show();
//                    Log.e(TAG, "Unable to upload file from the given uri", e);
//                }
//            }
//
//
//
//        }
//    }

    /*
     * Begins to upload the file specified by the file path.
     */
    private void beginUpload(String filePath) {
        if (filePath == null) {
            Toast.makeText(this, "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }

        if (!Internet.isConnected(this)) {
            SnackBarr.showErrorSnackBar(rlSelectFile, this, "No internet connection.");
            return;
        }

        rlSelectFile.setEnabled(false);
        rlUploadFile.setEnabled(false);
        llProgress.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setProgress(0);
        tvProgress.setText("Starting Upload..");

        //        Intent intent = new Intent(this, UploadService.class);
//        intent.putExtra("file_path", filePath);
//        startService(intent);

        File file = new File(filePath);
        TransferObserver observer = transferUtility.upload(
                Constants.BUCKET_NAME + SharedPreference.getLoginUserEmail(this), //Bucket Name
                String.valueOf(System.currentTimeMillis())+".mp4", //File Name in bucket ~ //file.getName() or //System.currentTimeMillis()
                file //File you want to upload to bucket
        );
        //         * Note that usually we set the transfer listener after initializing the
//         * transfer. However it isn't required in this sample app. The flow is
//         * click upload button -> start an activity for image selection
//         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
//         * -> set listeners to in progress transfers.
        observer.setTransferListener(new UploadListener());
    }


    private void requestStorageAccess() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        openExplorer();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied())
                        {
                            showSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showSettings() {
        AlertDialog.Builder builder=new AlertDialog.Builder(getApplicationContext());
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature.");
        builder.setPositiveButton("SETTINGS", (dialog, which) -> {
            dialog.cancel();
            Intent intent=new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri=Uri.fromParts("package",getPackageName(),null);
            intent.setData(uri);
            startActivityForResult(intent,101);
        });
        builder.setNegativeButton("Cancel",  null);
        builder.show();

    }

    private void openExplorer() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("video/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select videos"),UPLOAD_REQUEST_CODE);

    }

    //    @Override
//    public void onError(String text) {
//
//        tvProgress.setText("Error during upload: "+ text);
//        progressBar.setVisibility(View.GONE);
//        rlUploadFile.setEnabled(true);
//        rlSelectFile.setEnabled(true);
//
//    }
//
//    @Override
//    public void onProgressChanged(long bytesCurrent, long bytesTotal) {
//        //tvProgress.setText(String.format(Locale.ENGLISH, "onProgressChanged: %d, total: %d, current: %d", id, bytesTotal, bytesCurrent));
//
//        int percentage = ((int) bytesCurrent * 100) / (int) bytesTotal;
//        if (!FailCheck) {
//            tvProgress.setText(
//                    String.format(Locale.ENGLISH,
//                    "Uploading %d of %s videos..",
//                    UploadCount + 1, Constants.NoOfFiles));
//        }
//        progressBar.setMax((int) bytesTotal);
//        progressBar.setProgress((int) bytesCurrent);
//    }
//
//    @Override
//    public void onStateChanged(TransferState newState) {
//        //tvProgress.setText(String.format(Locale.ENGLISH, "onProgressChanged: %d, total: %d, current: %d", id, bytesTotal, bytesCurrent));
//        if (newState.name().equalsIgnoreCase("completed") ) {
//
//            if (FailCheck) {
//                progressBar.setVisibility(View.GONE);
//                tvProgress.setText("Upload Failed. Video file format not supported/Slow internet connection");
//            } else {
//                ++UploadCount;
//                tvProgress.setText(
//                        String.format(Locale.ENGLISH,
//                                "Uploading %d of %s videos..",
//                                UploadCount + 1, Constants.NoOfFiles));
//
//                if (UploadCount == Integer.parseInt(Constants.NoOfFiles)) {
//                    tvSelectedFile.setText("");
//                    llProgress.setVisibility(View.GONE);
//                    tvProgress.setText("Upload Completed Successfully.");
//                    Toast.makeText(SelectFile.this, UploadCount+ " videos uploaded successfully.", Toast.LENGTH_SHORT).show();
//                    startActivity(new Intent(SelectFile.this, Submit.class));
//                }
//            }
//        }
//        if (newState.name().equalsIgnoreCase("failed")) {
//            FailCheck = true;
//            rlUploadFile.setEnabled(true);
//            progressBar.setVisibility(View.GONE);
//            tvProgress.setText("Upload Failed. Video file format not supported/Slow internet connection ");
//        }
//
//    }


    /*
     * A TransferListener class that can listen to a upload task and be notified
     * when the status changes.
     */
    public class UploadListener implements TransferListener {

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "Error during upload: " + id, e);
            Toast.makeText(SelectFile.this, "errorr....>"+e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            tvProgress.setText("Error during upload: "+ e.getLocalizedMessage());
            progressBar.setVisibility(View.GONE);
            rlUploadFile.setEnabled(true);
            rlSelectFile.setEnabled(true);
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
            //tvProgress.setText(String.format(Locale.ENGLISH, "onProgressChanged: %d, total: %d, current: %d", id, bytesTotal, bytesCurrent));

            int percentage = ((int) bytesCurrent * 100) / (int) bytesTotal;
            if (!FailCheck) {
                tvProgress.setText(String.format(Locale.ENGLISH, "Uploading %d of %s videos..",
                        UploadCount + 1, Constants.NoOfFiles));
            }
            progressBar.setMax((int) bytesTotal);
            progressBar.setProgress((int) bytesCurrent);

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.d(TAG, "onStateChanged: " + id + ", " + newState);

            if (newState.name().equalsIgnoreCase("completed") ) {

                if (FailCheck) {
                    progressBar.setVisibility(View.GONE);
                    tvProgress.setText(getString(R.string.upload_failed_text));
                } else {
                    ++UploadCount;
                    tvProgress.setText(
                            String.format(Locale.ENGLISH,
                                    "Uploading %d of %s videos..",
                                    UploadCount + 1, Constants.NoOfFiles));

                    if (UploadCount == Integer.parseInt(Constants.NoOfFiles)) {
                        tvSelectedFile.setText("");
                        llProgress.setVisibility(View.GONE);
                        tvProgress.setText("Upload Completed Successfully.");
                        Toast.makeText(SelectFile.this, UploadCount+ " videos uploaded successfully.", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(SelectFile.this, Submit.class));
                        finish();
                    }
                }
                rlSelectFile.setEnabled(true);
                rlUploadFile.setEnabled(true);
            }
            if (newState.name().equalsIgnoreCase("failed")) {
                FailCheck = true;
                rlSelectFile.setEnabled(true);
                rlUploadFile.setEnabled(true);
                progressBar.setVisibility(View.GONE);
                tvProgress.setText(R.string.upload_failed_text);
            }

        }
    }

    //--------------------- -----------[]-----------------------------------------

    //***************************[Multiple Files]*******************

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                selectedVideos = getSelectedVideos(data);
                Constants.NoOfFiles = String.valueOf(selectedVideos.size());
                StringBuilder filename = new StringBuilder();
                for (int i=0;i<selectedVideos.size();i++)
                {
                    String path=selectedVideos.get(i);
                    filename.append(path.substring(path.lastIndexOf("/") + 1));
                }
                if (selectedVideos.size()>1)
                {
                    tvSelectedFile.setText(String.valueOf(selectedVideos.size()+" videos selected"));
                }
                else {
                    tvSelectedFile.setText(filename.toString());

                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),"Please try again with default file manager",Toast.LENGTH_LONG).show();
            }

//

        }
//        finish();
    }

    private List<String> getSelectedVideos(Intent data) {

        List<String> result = new ArrayList<>();

        ClipData clipData = data.getClipData();
        if(clipData != null) {
            for(int i=0;i<clipData.getItemCount();i++) {
                ClipData.Item videoItem = clipData.getItemAt(i);
                Uri videoURI = videoItem.getUri();
                String filePath = getPath(this, videoURI);
                result.add(filePath);
            }
        }
        else {
            Uri videoURI = data.getData();
            String filePath = getPath(this, videoURI);
            result.add(filePath);
        }

        return result;
    }
    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
            else if ("content".equalsIgnoreCase(uri.getScheme())) {

                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();

                //19122019
                return getImageRealPath(context.getContentResolver(),uri, null);
                //return getDataColumn(context, uri, null, null); //commented 19122019
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            //19122019
            return getImageRealPath(context.getContentResolver(),uri, null);
            //return getDataColumn(context, uri, null, null); //commented 19122019
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    private void showLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SelectFile.this);
        builder.setTitle("Pixona");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreference.loggedOut(SelectFile.this);
                startActivity(new Intent(SelectFile.this, Login.class));
                finish();
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    //19122019
    /* Return uri represented document file real local path.*/
    private static String getImageRealPath(ContentResolver contentResolver, Uri uri, String whereClause) {
        String ret = "";

        // Query the uri with condition.
        Cursor cursor = contentResolver.query(uri, null, whereClause, null, null);

        if (cursor != null) {
            boolean moveToFirst = cursor.moveToFirst();
            if (moveToFirst) {

                // Get columns name by uri type.
                String columnName = MediaStore.Images.Media.DATA;

                if (uri == MediaStore.Images.Media.EXTERNAL_CONTENT_URI) {
                    columnName = MediaStore.Images.Media.DATA;
                } else if (uri == MediaStore.Audio.Media.EXTERNAL_CONTENT_URI) {
                    columnName = MediaStore.Audio.Media.DATA;
                } else if (uri == MediaStore.Video.Media.EXTERNAL_CONTENT_URI) {
                    columnName = MediaStore.Video.Media.DATA;
                }

                // Get column index.
                int imageColumnIndex = cursor.getColumnIndex(columnName);

                if (imageColumnIndex > -1) {// Get column value which is the uri related file local path.
                    ret = cursor.getString(imageColumnIndex);
                } else {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        ret = Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                    //Added to fix issue in note 19
                    else if(split.length == 1 ) {
                        ret = Environment.getExternalStorageDirectory() + "/" + split[0];
                    }
                }
            }
        }

        return ret;
    }

}
