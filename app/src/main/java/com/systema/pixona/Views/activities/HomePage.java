package com.systema.pixona.Views.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.systema.pixona.R;
import com.systema.pixona.Utils.Constants;
import com.systema.pixona.Utils.SharedPreference;

public class HomePage extends AppCompatActivity {

    private ImageView ivVideoIdeas, ivRecord, ivUpload;
    private TextView tvContactUs, tvNeedHelp;
    private ImageView ivSettings;


    private void init() {
        ivVideoIdeas = findViewById(R.id.homePage_ivVideoIdeas);
        ivRecord = findViewById(R.id.homePage_ivRecord);
        ivUpload = findViewById(R.id.homePage_ivUpload);
        tvContactUs = findViewById(R.id.homePage_tvContactUs);
        tvNeedHelp = findViewById(R.id.homePage_tvNeedHelp);
        ivSettings = findViewById(R.id.homePage_ivSettings);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        init();
//        Crashlytics.getInstance().crash(); // Force a crash

        if (SharedPreference.getUserStatus(this).equalsIgnoreCase(Constants.DEACTIVATE)) {
            Log.e("HomePage", "deactive");
            SharedPreference.loggedOut(HomePage.this);
        } else {
            Log.e("HomePage", "active");
        }

        View.OnClickListener listener = v -> startActivity(new Intent(HomePage.this, ContactUs.class));

        tvContactUs.setOnClickListener(listener);
        tvNeedHelp.setOnClickListener(listener);

        ivVideoIdeas.setOnClickListener(v -> startActivity(new Intent(HomePage.this, IdeasMenu.class)));

        ivRecord.setOnClickListener(v -> startActivity(new Intent(HomePage.this, NewTeleprompter.class)));

        ivUpload.setOnClickListener(v -> startActivity(new Intent(HomePage.this, Login.class)));

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SharedPreference.getLoginStatus(this)) {
            ivSettings.setVisibility(View.VISIBLE);
            ivSettings.setOnClickListener(v -> showPopUp());
        } else {
            ivSettings.setVisibility(View.GONE);
        }

    }

    private void showPopUp() {
        PopupMenu popupMenu;

        popupMenu = new PopupMenu(this, ivSettings);
        popupMenu.getMenuInflater().inflate(R.menu.settings, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.menu_changePassword:
                    startActivity(new Intent(HomePage.this, ChangePassword.class));
                    break;
                case R.id.menu_logout:
                    AlertDialog.Builder builder = new AlertDialog.Builder(HomePage.this);
                    builder.setTitle("Pixona");
                    builder.setMessage("Are you sure you want to logout?");
                    builder.setPositiveButton("Yes", (dialog, which) -> {
                        SharedPreference.loggedOut(HomePage.this);
                        Toast.makeText(this, "Logged out..", Toast.LENGTH_SHORT).show();
                        onResume();
                    });
                    builder.setNegativeButton("No", null);
                    builder.show();
                default:
                    break;

            }
            return true;
        });
        popupMenu.show();
    }

}
