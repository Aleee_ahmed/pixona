package com.systema.pixona.Views.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.systema.pixona.R;

public class AdminMenu extends AppCompatActivity {

    private LinearLayout linearLayoutUsers, linearLayoutUploadVideoScript, linearLayoutPushNotifications;


    private void init() {

        linearLayoutUploadVideoScript = findViewById(R.id.adminMenu_linearLayoutUploadVideoScript);
        linearLayoutUsers = findViewById(R.id.adminMenu_linearLayoutUser);
        linearLayoutPushNotifications = findViewById(R.id.adminMenu_linearLayoutPushNotifications);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_menu);

        init();


        linearLayoutUsers.setOnClickListener(v ->
                startActivity(new Intent(AdminMenu.this, SearchUsers.class))
        );

        linearLayoutUploadVideoScript.setOnClickListener(v ->
                startActivity(new Intent(AdminMenu.this, AdminVideoSaving.class))
        );

        linearLayoutPushNotifications.setOnClickListener(v ->
                startActivity(new Intent(AdminMenu.this, PushNotification.class))
        );

    }


}
