package com.systema.pixona.Views.activities;

import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.systema.pixona.R;
import com.systema.pixona.Utils.Util;

public class Thanks extends AppCompatActivity {
    private TextView tvMessage,tvSubMessage,tvSubMessage2;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanks);
        initViews();
        startTimer();
        String result = getIntent().getStringExtra("From");

        if (result !=null){
            if (result.equalsIgnoreCase("ContactUs")) {
                tvMessage.setTextSize(TypedValue.COMPLEX_UNIT_SP, Util.dpToPx(7));
                tvMessage.setText("Thank You!");
                tvSubMessage.setText("We will be in contact shortly!");
                tvSubMessage2.setVisibility(View.GONE);
//                startTimer(false);
            } else if (result.equalsIgnoreCase("SignUp")) {
                tvMessage.setText("Thank You for Signing Up!");
                tvSubMessage.setText("Please check your email for further instructions and be sure to watch our \"How to\" video!");
                tvSubMessage2.setVisibility(View.GONE);
//                startTimer(false);

            } else if (result.equalsIgnoreCase("ForgetPassword")) {
                tvMessage.setText("Password Reset Email Sent!");
                tvSubMessage.setText("We received your request for a password reset link.");
                tvSubMessage2.setVisibility(View.VISIBLE);
                tvSubMessage2.setText("You should receive the email soon");
//                startTimer(false);


            }
        }

    }

    private void initViews() {
        tvMessage=findViewById(R.id.thanks_tvMassage);
        tvSubMessage=findViewById(R.id.thanks_tvsubMessage);
        tvSubMessage2=findViewById(R.id.thanks_tvsubMessage2);
    }

    private void startTimer(){
        countDownTimer = new CountDownTimer(6000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }
            @Override
            public void onFinish() {
                finish();
                countDownTimer.cancel();
            }
        }.start();

    }
}
