package com.systema.pixona.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

import static com.systema.pixona.Utils.Constants.TXT_EXTENSION;

public class SaveVideoinGallery  extends AsyncTask<Void, Void, Boolean> {

    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private String mNextVideoAbsolutePath;


    public SaveVideoinGallery(Context mContext, String mNextVideoAbsolutePath) {            
        this.mContext = mContext;
        this.mNextVideoAbsolutePath = mNextVideoAbsolutePath;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        Long currentTs = System.currentTimeMillis() / 1000;
        String timestamp = currentTs.toString();

        String filename = timestamp + ".mp4";
        FileOutputStream outputStream;
        File file;

        try {
            file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), filename);
            outputStream = new FileOutputStream(file);
            outputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        String status;
        if (aBoolean) {
            status = ("Download Successfull");
        } else {
            status = ("Download failed. Please try later");
        }
        Toast.makeText(mContext, status, Toast.LENGTH_SHORT).show();
    }
}
