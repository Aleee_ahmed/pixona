/*
 * Copyright 2015-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.systema.pixona.Utils;


import android.util.Base64;

import java.util.Locale;

public class Constants {

    /*
     * You should replace these values with your own. See the README for details
     * on what to fill in.
     */
    public static final String COGNITO_POOL_ID = "us-east-2:95566b2f-9ff7-4998-aab5-4853060dcf17";

    public static final String BASE_URL = "https://www.googleapis.com/youtube/v3/";
    public static final String BASE_URL_PATH = "videos";

    public static final String BASE_URL_EMAIL = "https://xunami.net/";
    public static final String BASE_URL_PATH_EMAIL = "send_email.php";

    public static final String EMAIL = "support@xunami.net";
    public static final String ADMIN = "test@test.com";

    public static final String ACTIVE = "active";
    public static final String PENDING = "pending";
    public static final String DEACTIVATE = "deactive";

    /*
     * Region of your Cognito identity pool ID.
     */
    public static final String COGNITO_POOL_REGION = "us-east-2";

    /*
     * Note, you must first createVideoDetailsJSON a bucket using the S3 console before running
     * the sample (https://console.aws.amazon.com/s3/). After creating a bucket,
     * put it's name in the field below.
     */
    public static final String BUCKET_NAME = "pixona/upload/";

    /*
     * Region of your bucket.
     */
    public static final String BUCKET_REGION = "us-east-1";



    public static final String API_KEY="Bearer un7d3araet2i5m35z33xhyqwe1";

    //Sheet ID
    public static final String SHEET_ID="6893456468535172";

    //Columns ID's for Uploaded Video Details

    public static final String COLOUMN_NAME_ID = "13871697160068";

    public static final String COLOUMN_TEMPLATE_VIDEO_TITLE_ID = "8961042840610692";

    public static final String COLOUMN_NOFILES_ID = "4517471324530564";

    public static final String COLOUMN_GUESTNAME_ID = "2265671510845316";

    public static final String COLOUMN_NOTES_ID = "6769271138215812";

    public static final String COLOUMN_COMPANY_ID = "1139771604002692";

    /* Column id's for SIgn up smart sheet */

    public static final String COLOUMN_SIGNUP_NAME_ID = "5055778915673988";

    public static final String COLOUMN_EMAIL_ID = "4545639880124292";

    public static final String COLOUMN_PASSWORD_ID = "2803979101988740";

    public static final String COLOUMN_CREDIT_CARD_NUMBER_ID = "7307578729359236";

    public static final String COLOUMN_CVV_ID = "1678079195146116";

    public static final String COLOUMN_ZIP_ID = "6181678822516612";

    public static final String COLOUMN_EXP_DATE_ID = "3929879008831364";

    public static final String COLOUMN_PROMO_CODE_ID = "494826023085956";


    public static String NoOfFiles;

    public static final String Youtube_API_KEY="AIzaSyAtrt713XaPdEyBGxY5YVbZOJ4-FzX1WtI";


    public static String encode(String value) {
        value = value.toLowerCase().trim();
        String email = Base64.encodeToString(value.getBytes(), Base64.DEFAULT);
        if (email.endsWith("\n")) {
            email = email.substring(0, email.length()-1);
        }
        return email;
    }

    public static String encodeVideoId(String value) {
        value = value.trim();
        String email = Base64.encodeToString(value.getBytes(), Base64.DEFAULT);
        if (email.endsWith("\n")) {
            email = email.substring(0, email.length()-1);
        }
        return email;
    }

    public static String decode(String value) {
        return new String(Base64.decode(value, Base64.DEFAULT));

    }


    public static String splitVideoIdFromURL(String url) {

        if (url.contains("v=")) {
            if (url.contains("&")) {
                String[] u = url.split("v=");
                u = u[1].split("&");
                url = u[0];
            } else {
                String[] u = url.split("v=");
                url = u[1];
            }
        } else {
            String[] u = url.split("be/");
            url = u[1];
        }
        return encodeVideoId(url);
    }


    public static String getYoutubeThumbnail(String videoKey){
        return "https://img.youtube.com/vi/" + decode(videoKey) + "/maxresdefault.jpg";
    }

    public static String getDuration(String dur) {
        return dur.replace("PT","").replace("H",":").replace("M",":").replace("S","");
    }


    public static class KEYS {
        public static final String VIDEO_ID = "id";
        public static final String PART = "part";
        public static final String API_KEY = "key";
        public static final String API_TO = "to";
        public static final String API_FROM = "from";
        public static final String API_SUBJECT = "subject";
        public static final String API_TEXT = "text";
    }

    public static class VALUES {
        public static final String PART = "contentDetails";
        public static final String FROM = "Xunami <pixonainfo@gmail.com>";
        public static final String TO = "Xunami <" + EMAIL +">";

    }

    //Teleprompter
    public static final String IMAGE_DATA = "IMAGE_DATA";
    public static final String EXTRA_FILE_DATA = "EXTRA_FILE_DATA";
    public static final String SHARED_PREF_FILE = "SHARED_PREF_FILE";
    public static final String TXT_EXTENSION = ".txt";
    public static final String PATH_LICENSES_FILE = "file:///android_asset/licenses.html";
    public static final String DIALOG_TAG_LICENSES = "DIALOG_TAG_LICENSES";
    public static final String INTENT_EXTRA_CONTENT = "content";

}













