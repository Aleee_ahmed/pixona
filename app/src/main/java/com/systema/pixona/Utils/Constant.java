/*
 * Copyright 2015-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.systema.pixona.Utils;

public class Constant {

    /*
     * You should replace these values with your own. See the README for details
     * on what to fill in.
     */
    public static final String COGNITO_POOL_ID = "us-east-1:08d79448-385f-43da-8c57-9b1eb7d6e987";

    /*
     * Region of your Cognito identity pool ID.
     */
    public static final String COGNITO_POOL_REGION = "us-east-1";

    /*
     * Note, you must first createVideoDetailsJSON a bucket using the S3 console before running
     * the sample (https://console.aws.amazon.com/s3/). After creating a bucket,
     * put it's name in the field below.
     */
    public static final String BUCKET_NAME = "pixona001/upload/";

    /*
     * Region of your bucket.
     */
    public static final String BUCKET_REGION = "ap-south-1";

    public static final String API_KEY="Bearer un7d3araet2i5m35z33xhyqwe1";

    //Sheet ID
    public static final String SHEET_ID="6893456468535172";

    //Coloumns ID's

    public static final String COLOUMN_NAME_ID="13871697160068";

    public static final String COLOUMN_NOFILES_ID="4517471324530564";

    public static final String COLOUMN_GUESTNAME_ID="2265671510845316";

    public static final String COLOUMN_NOTES_ID="6769271138215812";

    public static final String COLOUMN_COMPANY_ID="1139771604002692";

    public static String NoOfFiles;
}
