package com.systema.pixona.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {

    private static final String LOGIN = "login";
    private static final String LOGGED_IN = "logged_in";
    private static final String NAME = "name";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";
    private static final String USER_STATUS = "status";

    public static void save(Context context, String name, String password, String email) {
        SharedPreferences preferences = context.getSharedPreferences(LOGIN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(NAME, name);
        editor.putString(PASSWORD, password);
        editor.putString(EMAIL, Constants.decode(email));
        editor.putBoolean(LOGGED_IN, true);
        editor.apply();
    }

    public static void setUserStatus(Context context, String status) {
        SharedPreferences preferences = context.getSharedPreferences(LOGIN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_STATUS, status);
        editor.apply();
    }

    public static String getUserStatus(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(LOGIN, Context.MODE_PRIVATE);
        return preferences.getString(USER_STATUS, "active");
    }



    public static void loggedOut(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(LOGIN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(LOGGED_IN, false);
        editor.apply();
    }

    public static boolean getLoginStatus(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(LOGIN, Context.MODE_PRIVATE);
        return preferences.getBoolean(LOGGED_IN, false);
    }


    public static String getLoginUserEmail(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(LOGIN, Context.MODE_PRIVATE);
        return preferences.getString(EMAIL,"");
    }


    public static String getLoginUserPassword(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(LOGIN, Context.MODE_PRIVATE);
        return preferences.getString(PASSWORD,"");
    }


}
