package com.systema.pixona.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class Json {

    public static String createVideoDetailsJSON(
            String YourName, String NumberOfFiles, String templateName,
            String GuestName, String Notes, String Company) {
        try {
            //JSONArray MainArray = new JSONArray();
            JSONArray jsonArray = new JSONArray();

            JSONObject cell1 = new JSONObject();
            JSONObject cell2 = new JSONObject();
            JSONObject cell3 = new JSONObject();
            JSONObject cell4 = new JSONObject();
            JSONObject cell5 = new JSONObject();
            JSONObject cell6 = new JSONObject();

            cell1.put("columnId", Constants.COLOUMN_NAME_ID);
            cell1.put("value", YourName);
            cell1.put("displayValue", YourName);

            cell6.put("columnId", Constants.COLOUMN_TEMPLATE_VIDEO_TITLE_ID);
            cell6.put("value", templateName);
            cell6.put("displayValue", templateName);

            cell2.put("columnId", Constants.COLOUMN_NOFILES_ID);
            cell2.put("value", NumberOfFiles);
            cell2.put("displayValue", NumberOfFiles);

            cell3.put("columnId", Constants.COLOUMN_GUESTNAME_ID);
            cell3.put("value", GuestName);
            cell3.put("displayValue", GuestName);

            cell4.put("columnId", Constants.COLOUMN_NOTES_ID);
            cell4.put("value", Notes);
            cell4.put("displayValue", Notes);

            cell5.put("columnId", Constants.COLOUMN_COMPANY_ID);
            cell5.put("value", Company);
            cell5.put("displayValue", Company);

            JSONObject jsonObject = new JSONObject();

            jsonArray.put(cell1);
            jsonArray.put(cell2);
            jsonArray.put(cell3);
            jsonArray.put(cell4);
            jsonArray.put(cell5);
            jsonArray.put(cell6);

            jsonObject.put("toTop", true);
            jsonObject.put("cells", jsonArray);

            //MainArray.put(jsonObject);

            return jsonObject.toString();

        } catch (Exception e) {
            return null;
        }

    }

    public static String createSignUpJSON(
            String YourName, String Email, String Password, String CardNumber,
            String CVV,  String Zip, String CardExpiration, String promoCode ) {
        try {
            //JSONArray MainArray = new JSONArray();
            JSONArray jsonArray = new JSONArray();

            JSONObject cell1 = new JSONObject();
            JSONObject cell2 = new JSONObject();
            JSONObject cell3 = new JSONObject();
            JSONObject cell4 = new JSONObject();
            JSONObject cell5 = new JSONObject();
            JSONObject cell6 = new JSONObject();
            JSONObject cell7 = new JSONObject();
            JSONObject cell8 = new JSONObject();

            cell1.put("columnId", Constants.COLOUMN_SIGNUP_NAME_ID);
            cell1.put("value", YourName);
            cell1.put("displayValue", YourName);

            cell2.put("columnId", Constants.COLOUMN_EMAIL_ID);
            cell2.put("value", Email);
            cell2.put("displayValue", Email);

            cell3.put("columnId", Constants.COLOUMN_PASSWORD_ID);
            cell3.put("value", Password);
            cell3.put("displayValue", Password);

            cell4.put("columnId", Constants.COLOUMN_CREDIT_CARD_NUMBER_ID);
            cell4.put("value", CardNumber);
            cell4.put("displayValue", CardNumber);

            cell5.put("columnId", Constants.COLOUMN_CVV_ID);
            cell5.put("value", CVV);
            cell5.put("displayValue", CVV);

            cell6.put("columnId", Constants.COLOUMN_ZIP_ID);
            cell6.put("value", Zip);
            cell6.put("displayValue", Zip);

            cell7.put("columnId", Constants.COLOUMN_EXP_DATE_ID);
            cell7.put("value", CardExpiration);
            cell7.put("displayValue", CardExpiration);

            cell8.put("columnId", Constants.COLOUMN_PROMO_CODE_ID);
            cell8.put("value", promoCode);
            cell8.put("displayValue", promoCode);

            JSONObject jsonObject = new JSONObject();

            jsonArray.put(cell1);
            jsonArray.put(cell2);
            jsonArray.put(cell3);
            jsonArray.put(cell4);
            jsonArray.put(cell5);
            jsonArray.put(cell6);
            jsonArray.put(cell7);
            jsonArray.put(cell8);

            jsonObject.put("toTop", true);
            jsonObject.put("cells", jsonArray);

            //MainArray.put(jsonObject);

            return jsonObject.toString();

        } catch (Exception e) {
            return null;
        }

    }

}
