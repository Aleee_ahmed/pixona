package com.systema.pixona.Utils;

import android.app.Application;
import android.util.Log;

import com.appsflyer.AppsFlyerInAppPurchaseValidatorListener;
import com.appsflyer.AppsFlyerLib;
import com.appsflyer.AppsFlyerConversionListener;
import java.util.Map;

public class Xunami extends Application {

    private static final String AF_DEV_KEY = "NX2z4Er222ox5JDfiiNV7f";


    @Override
    public void onCreate() {
        super.onCreate();

        AppsFlyerConversionListener conversionListener = new AppsFlyerConversionListener() {


            @Override
            public void onInstallConversionDataLoaded(Map<String, String> conversionData) {

                for (String attrName : conversionData.keySet()) {
                    Log.d("LOG_TAG", "attribute: " + attrName + " = " + conversionData.get(attrName));
                }
            }

            @Override
            public void onInstallConversionFailure(String errorMessage) {
                Log.d("LOG_TAG", "error getting conversion data: " + errorMessage);
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> conversionData) {

                for (String attrName : conversionData.keySet()) {
                    Log.d("LOG_TAG", "attribute: " + attrName + " = " + conversionData.get(attrName));
                }

            }

            @Override
            public void onAttributionFailure(String errorMessage) {
                Log.d("LOG_TAG", "error onAttributionFailure : " + errorMessage);
            }
        };

        AppsFlyerLib.getInstance().init(AF_DEV_KEY, conversionListener, getApplicationContext());
        AppsFlyerLib.getInstance().startTracking(this);

        AppsFlyerLib.getInstance().registerValidatorListener(this,
                new AppsFlyerInAppPurchaseValidatorListener() {
                    public void onValidateInApp() {
                        Log.d("TRACKING", "Purchase validated successfully");
                    }
                    public void onValidateInAppFailure(String error) {
                        Log.d("TRACKING", "onValidateInAppFailure called: " + error);
                    }
                });


    }
}
