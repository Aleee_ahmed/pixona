package com.systema.pixona.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.LinearInterpolator;
import android.widget.Scroller;


/**
 * A TextView that scrolls it contents across the screen, in a similar fashion as movie credits roll
 * across the theater screen.
 *
 * @author Matthias Kaeppler
 *
 */
public class ScrollingTextView extends androidx.appcompat.widget.AppCompatTextView implements Runnable {

    private static final float DEFAULT_SPEED = 15.0f;

    private Scroller scroller;
    private float speed = DEFAULT_SPEED;
    private boolean continuousScrolling = false;
    private boolean flag = false;
    private int offset;
    private int distance;
    private int duration;

    public ScrollingTextView(Context context) {
        super(context);
        setup(context);
        Log.e("ScrollingTextView", "ScrollingTextView");
    }

    public ScrollingTextView(Context context, AttributeSet attributes) {
        super(context, attributes);
        setup(context);

    }

    private void setup(Context context) {
        scroller = new Scroller(context, new LinearInterpolator());
        setScroller(scroller);
        Log.e("ScrollingTextView", "setup");
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (scroller.isFinished()) {
            scroll();
            if (flag)startScrolling();
        }
    }

    private void scroll() {
        int viewHeight = getHeight();
        int visibleHeight = viewHeight - getPaddingBottom() - getPaddingTop();
        int lineHeight = getLineHeight();

        offset = -1 * visibleHeight;
        distance = visibleHeight + getLineCount() * lineHeight;
        duration = (int) (distance * speed);

        if (continuousScrolling) {
            post(this);
        }
    }

    public void startScrolling() {
        setContinuousScrolling(true);
        scroller.startScroll(0, offset, 0, distance, duration);
    }

    @Override
    public void run() {
        if (scroller.isFinished()) {
            scroll();
            if (flag) startScrolling();
        } else {
            post(this);
        }
    }

    public void setSpeed(int speed) {
        this.speed = speed;
        scroll();
        if (flag) startScrolling();
    }

    public float getSpeed() {
        return speed;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setContinuousScrolling(boolean continuousScrolling) {
        this.continuousScrolling = continuousScrolling;
    }

    public boolean isContinuousScrolling() {
        return continuousScrolling;
    }
}