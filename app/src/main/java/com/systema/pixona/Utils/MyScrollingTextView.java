package com.systema.pixona.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.LinearInterpolator;
import android.widget.Scroller;
import android.widget.TextView;

import com.systema.pixona.R;
import com.systema.pixona.Views.Fragments.CameraFragment;

@SuppressLint({"AppCompatCustomView"})
public class MyScrollingTextView extends TextView {

    private static final float DEFAULT_SPEED = 10.0f;
    public static Scroller scroller;
    private String TAG;
    public Context contxt;
    public boolean mPaused;
    public int offset;
    private int pixelCount;
    public float speed;

    public interface OnStopRecording {
        void OnStopRecording(String str, String str2);
    }

    public MyScrollingTextView(Context context) {
        super(context);
        this.TAG = "VerticalMarqueeTvCustome::";
        this.speed = DEFAULT_SPEED;
        this.offset = 0;
        this.mPaused = false;
        this.contxt = context;
        init(null, 0);
        scrollerInstance(context);
    }

    public MyScrollingTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.TAG = "VerticalMarqueeTvCustome::";
        this.speed = DEFAULT_SPEED;
        this.offset = 0;
        this.mPaused = false;
        init(attributeSet, 0);
        scrollerInstance(context);
        this.contxt = context;
    }

    @RequiresApi(api = 21)
    public MyScrollingTextView(Context context, @Nullable AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.TAG = "VerticalMarqueeTvCustome::";
        this.speed = 1092616192;
        this.offset = 0;
        this.mPaused = false;
        init(attributeSet, i);
        scrollerInstance(context);
        this.contxt = context;
    }

    private void init(AttributeSet attributeSet, int i) {
        m846u(getContext().obtainStyledAttributes(attributeSet, R.styleable.MyTextView, i, 0));
    }

    /* renamed from: u */
    protected void m846u(TypedArray typedArray) {
//        typedArray = typedArray.getString(2);
//        if (!typedArray.equals("")) {
//            setTypeface(Typeface.createFromAsset(getContext().getAssets(), typedArray));
//        }

    }

    public void scrollerInstance(Context context) {
        scroller = new Scroller(context, new LinearInterpolator());
        setScroller(scroller);
    }

    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (scroller.isFinished()) {
            scroll();
        }
    }

    public void scroll() {
        int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
        int lineHeight = getLineHeight();
        if (this.offset == 0) {
            this.offset = -1 * height;
        } else {
            String str = this.TAG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("OffSet::");
            stringBuilder.append(this.offset);
        }
        int lineCount = height + (getLineCount() * lineHeight);
        int i = (int) (((float) lineCount) * this.speed);
        if (this.mPaused) {
            this.mPaused = false;
        }
        scroller.startScroll(0, this.offset, 0, lineCount, i);

        String str2 = this.TAG;
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("RowCount::");
        stringBuilder2.append(getLineCount());
        Log.d(str2, stringBuilder2.toString());
    }

    @RequiresApi(api = 21)
    public void computeScroll() {
        super.computeScroll();
        if (scroller != null && scroller.isFinished()) {
            scroll();
            CameraFragment cameraFragment = new CameraFragment();
            this.pixelCount = getLineHeight() * getLineCount();
            if (CameraFragment.mIsRecordingVideo) {
                if (this.mPaused) {
                    Log.d(this.TAG, "It Pause::");
                } else {
                    cameraFragment.OnStopRecording(CameraFragment.getSpeechID, CameraFragment.getScript);
                }
            }
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (scroller == null) {
        }
    }

    public void pauseScroll() {
        this.mPaused = true;
        this.offset = scroller.getCurrY();
        String str = this.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GetLineCount::");
        stringBuilder.append(this.offset);
        scroller.abortAnimation();
    }

    public void setSpeed(float f) {
        this.speed = f;
    }

    public float getSpeed() {
        return this.speed;
    }
}
