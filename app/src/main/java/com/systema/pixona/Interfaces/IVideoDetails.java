package com.systema.pixona.Interfaces;


import com.systema.pixona.Models.VideoDetailsModel;

public interface IVideoDetails {

    interface iVideoDetailsMain {
        void showProgress();
        void hideProgress();
        void displayAddTravelPlan(VideoDetailsModel videoDetailsModel);
        void displayError(Throwable throwable);
    }

    interface iVideoDetailsPresenter {
        void fetchVideoDetails(String videoId);
    }

}
