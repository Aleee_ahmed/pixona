package com.systema.pixona.Interfaces;

import com.systema.pixona.Models.SendEmailModel;

public interface ISendEmail {

    interface iSendEmailMain {
        void showProgress();
        void hideProgress();
        void displayResult(String model);
        void displayError(String error);
    }

    interface iSendEmailPresenter {
        void sendEmail(String email, String subject, String message);
    }

}
