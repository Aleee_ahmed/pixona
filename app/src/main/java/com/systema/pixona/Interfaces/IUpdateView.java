package com.systema.pixona.Interfaces;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;

public interface IUpdateView {

    void onError(String error);

    void onProgressChanged(long bytesCount, long bytesTotal);

    void onStateChanged(TransferState transferState);

}
