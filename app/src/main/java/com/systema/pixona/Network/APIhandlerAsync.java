package com.systema.pixona.Network;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Junaid Ali on 11/15/2017.
 */

public class APIhandlerAsync extends AsyncTask<Void,String,String> {


   private String url;


    @SuppressLint("StaticFieldLeak")
   private Context context;
    private AsyncResponse delegate;

    public APIhandlerAsync(Context context, String url, AsyncResponse delegate)
    {
        this.context=context;
        this.url=url;
        this.delegate=delegate;


    }


    @Override
    protected String doInBackground(Void... voids) {
        try {
            String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
            String url1 = Uri.encode(this.url, ALLOWED_URI_CHARS);
            URL uri = new URL(url1);
            HttpURLConnection connection = (HttpURLConnection) uri.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int c  = connection.getResponseCode();
            InputStream inputStream;
            if (c == HttpURLConnection.HTTP_OK){
                inputStream = connection.getInputStream();
            }else{
                inputStream = connection.getErrorStream();
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine())!=null){
                builder.append(line).append("\n");
            }
            return  builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
//        if (result.contains("Exception"))
//        {
//            Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
//        }
//        else
        {
            delegate.processFinish(result);
        }
    }

    public interface AsyncResponse{
        void processFinish(String output);
    }
}



