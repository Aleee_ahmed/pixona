package com.systema.pixona.Network;


import com.systema.pixona.Models.SendEmailModel;
import com.systema.pixona.Models.VideoDetailsModel;
import com.systema.pixona.Utils.Constants;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RetrofitInterface {

//******************************************************************************************************************************************************
    /* Get Video Details  */
//******************************************************************************************************************************************************

    @GET(Constants.BASE_URL_PATH)
    Observable<VideoDetailsModel> getVideoDetails(
            @Query(Constants.KEYS.VIDEO_ID) String videoId,
            @Query(Constants.KEYS.PART) String part,
            @Query(Constants.KEYS.API_KEY) String apiKey);


    //******************************************************************************************************************************************************
    /* SEND Email  */
//******************************************************************************************************************************************************

    @FormUrlEncoded
    @POST(Constants.BASE_URL_PATH_EMAIL)
    Observable<String> sendEmail(
            @Field(Constants.KEYS.API_TO) String to,
            @Field(Constants.KEYS.API_SUBJECT) String subject,
            @Field(Constants.KEYS.API_TEXT) String message
    );


//******************************************************************************************************************************************************
//******************************************************************************************************************************************************


}
