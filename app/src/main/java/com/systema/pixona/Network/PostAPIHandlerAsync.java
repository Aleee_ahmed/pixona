package com.systema.pixona.Network;

import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;


public class PostAPIHandlerAsync extends AsyncTask<Void, String, String> {

    private String url;
    private String data;
    private String type;
    private PostAsyncResponse delegate;


    public PostAPIHandlerAsync(String url, String data, String type, PostAsyncResponse delegate) {
        this.url = url;
        this.data = data;
        this.delegate = delegate;
        this.type = type;

    }

    @Override
    protected String doInBackground(Void... voids) {
        try {
            URL url = new URL(this.url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(type);
            //connection.setRequestProperty("Authorization", "someAuthString");
            connection.setRequestProperty("Authorization", "Bearer un7d3araet2i5m35z33xhyqwe1");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Connection", "keep-alive");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.connect();


            OutputStream outputStream = connection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));

            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();

            InputStream stream;
            int code = connection.getResponseCode();
            if (code != HttpURLConnection.HTTP_OK) {
                stream = connection.getErrorStream();
            } else {
                stream = connection.getInputStream();
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line).append("\n");
            }

            return builder.toString();
        } catch (IOException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        assert result != null;
        delegate.postProcessFinished(result);

    }

    public interface PostAsyncResponse {
        void postProcessFinished(String output);
    }
}
