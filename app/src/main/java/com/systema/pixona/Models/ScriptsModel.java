package com.systema.pixona.Models;

public class ScriptsModel {

    private String scriptNumber, scriptName, scriptEdittedOn;

    public ScriptsModel(String scriptNumber, String scriptName, String scriptEdittedOn) {
        this.scriptNumber = scriptNumber;
        this.scriptName = scriptName;
        this.scriptEdittedOn = scriptEdittedOn;
    }

    public String getScriptNumber() {
        return scriptNumber;
    }

    public String getScriptName() {
        return scriptName;
    }

    public String getScriptEdittedOn() {
        return scriptEdittedOn;
    }
}
