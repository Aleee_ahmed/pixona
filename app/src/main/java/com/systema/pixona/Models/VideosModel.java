package com.systema.pixona.Models;

public class VideosModel {

    private String VideoId, VideoTitle, VideoDetails, ScriptUrl, VideoShots, VideoDuration, VideoCategory;

    public VideosModel(String videoId, String videoTitle,
                       String videoDetails, String scriptUrl,
                       String videoShots, String videoDuration, String videoCategory) {
        VideoId = videoId;
        VideoTitle = videoTitle;
        VideoDetails = videoDetails;
        ScriptUrl = scriptUrl;
        VideoShots = videoShots;
        VideoDuration = videoDuration;
        VideoCategory = videoCategory;
    }

    public String getVideoCategory() {
        return VideoCategory;
    }

    public String getVideoDuration() {
        return VideoDuration;
    }

    public String getVideoDetails() {
        return VideoDetails;
    }

    public String getScriptUrl() {
        return ScriptUrl;
    }

    public String getVideoShots() {
        return VideoShots;
    }

    public String getVideoId() {
        return VideoId;
    }

    public String getVideoTitle() {
        return VideoTitle;
    }
}
