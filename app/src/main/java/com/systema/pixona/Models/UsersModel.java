package com.systema.pixona.Models;

public class UsersModel {

    private String Name, Email,  JoiningDate, ExpirationDate, CreditCardNumber, CVV, Status, ZipCode;

    public UsersModel(String name,String email, String joiningDate, String expirationDate, String creditCardNumber, String cvv, String status, String zipCode) {
        Name = name;
        Email = email;
        JoiningDate = joiningDate;
        ExpirationDate = expirationDate;
        CreditCardNumber = creditCardNumber;
        CVV = cvv;
        Status = status;
        ZipCode = zipCode;
    }


    public String getName() {
        return Name;
    }

    public String getEmail() {
        return Email;
    }

    public String getJoiningDate() {
        return JoiningDate;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public String getCreditCardNumber() {
        return CreditCardNumber;
    }

    public String getCVV() {
        return CVV;
    }

    public String getStatus() {
        return Status;
    }

    public String getZipCode() {
        return ZipCode;
    }
}
