

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();


/* below code is used to send notification when data is added in notification table */
exports.sendNotification = functions.database.ref('/notifications/{notification_id}').onWrite((data, context) => {


  if (!data.after.exists()) {
    return;
  }
/* get all data from notification table - then fetch value of title */
  //const noti_id = context.params.notification_id;
  const value = data.after.val();
  const title = value.title;

  //console.log("Title: ", title);
  //console.log("Noti id: ", noti_id);

/* Creating Reference to 'Users' and 'Notifiactions' table. */
  var db = admin.database();
  var refUsers = db.ref('users');
  var refNotifications = db.ref('notifications');

/* below code only runs once to fetch all users deviceId */
  refUsers.once("value", function(snapshot) {

/* Creating array of device_ids to sendNotification to all at once */
    var device_ids = [];

    console.log("snapshot : ", snapshot.val()); //just to check value of snapshot (debugging)

/* below code get each node (used to get device_id of each user) */
    snapshot.forEach(function(trialSnapshot) {

    console.log("snapshot size : ", snapshot.val().length);


        var singleItem = trialSnapshot.val(); //get all data of single user
        var deviceId = singleItem.device_id; //get device id of user

        if (deviceId.length > 0 ) {
          console.log("Device Id : ", deviceId);
          console.log("singleItem : ", singleItem);

          device_ids.push(deviceId); //saving device id to device_ids array.

        }

    });

/* Creating notification to be send to all users. used title (fetched from notification table) as a body here */
    const payload = {
        notification:{
              title:"Xunami",
              body: title,
              icon:"default",
              sound: "default"
        }
    };
    //console.log("device_ids length : ", device_ids.length);

    console.log("device_ids  : ",device_ids);



/* sending notification to all device_ids */
    return admin.messaging().sendToDevice(device_ids, payload).then(response =>{

          /* removing notifications */
          return refNotifications.remove().then((response) => {
            return console.log('Notifiactions sent to user...', payload);
          });

        });
}, function (errorObject) {
  console.log("The read failed: " + errorObject.code); // runs if there is any error occured.
});


});
