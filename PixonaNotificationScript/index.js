

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.sendNotification = functions.database.ref('/notifications/{notification_id}').onWrite((data, context) => {

  //const noti_id = context.params.notification_id;
  const value = data.after.val();
  const title = value.title;

  //console.log("Title: ", title);
  //console.log("Noti id: ", noti_id);

  var db = admin.database();
  var ref = db.ref('users');

  ref.on("value", function(snapshot) {

    snapshot.forEach(function(trialSnapshot) {
        var singleItem = trialSnapshot.val();
        var deviceId = singleItem.device_id;

        console.log("Device Id : ", deviceId);
        console.log("singleItem : ", singleItem);

        if(deviceId.length > 0){

                  const payload = {
                      notification:{
                            title:"Xunami",
                            body: title,
                            icon:"default"
                      }
                  };

                 return admin.messaging().sendToDevice(deviceId, payload).then(response =>{
                         return console.log('Notifiactions sent to user...', payload);
                   });

        }

    });

}, function (errorObject) {
  console.log("The read failed: " + errorObject.code);
});


});
